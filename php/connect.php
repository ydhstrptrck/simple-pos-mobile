<?php
	$mysqli = new mysqli("localhost", "root", "", "simple_pos");
	if ($mysqli->connect_error) {
		die("Connection failed: " . $mysqli->connect_error);
	}

	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Expose-Headers", "Access-Control-*");
    header("Access-Control-Allow-Headers", "Access-Control-*, Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS, HEAD');
    header('Allow', 'GET, POST, PUT, DELETE, OPTIONS, HEAD');

	date_default_timezone_set('Asia/Bangkok');
?>