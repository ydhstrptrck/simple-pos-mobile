<?php 
	include "../connect.php";
    
	$sql = "SELECT * FROM store WHERE store_delete = 0";
	if(isset($_GET['keyword'])) {
		$sql .= " AND store_name LIKE '%" . $_GET['keyword'] . "%'";
	}
	$sql .= " ORDER BY store_name ASC";

	$result = $mysqli->query($sql);

	if ($result->num_rows > 0) {
		$store = array();
		$i = 0;
		while ($obj = $result->fetch_assoc()) {
			$store[$i]['store_id'] = addslashes(htmlentities($obj['store_id']));
			$store[$i]['store_name'] = addslashes(htmlentities($obj['store_name']));
			$store[$i]['store_address'] = addslashes(htmlentities($obj['store_address']));
			$i++;
	    }
		echo json_encode([
    		'status' => true,
    		'data' => $store
    	]);
	} else {
		echo json_encode([
    		'status' => false,
    		'message' => 'Tidak ada usaha ditemukan!'
    	]);
	}

	$mysqli->close();
?>