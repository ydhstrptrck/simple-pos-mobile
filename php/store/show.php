<?php 
	include "../connect.php";
    
    $store_id = filter_input(INPUT_POST, 'store_id', FILTER_SANITIZE_STRING);
    
	$sql = "SELECT * FROM store WHERE store_id = '$store_id'";
	$result = $mysqli->query($sql);
	
	if ($result->num_rows > 0) {
		$store = array();
		$i = 0;
		while ($obj = $result->fetch_assoc()) {
			$store[$i]['store_id'] = addslashes(htmlentities($obj['store_id']));
			$store[$i]['store_name'] = addslashes(htmlentities($obj['store_name']));
			$store[$i]['store_address'] = addslashes(htmlentities($obj['store_address']));
			$i++;
	    }
		echo json_encode([
    		'status' => true,
    		'data' => $store
    	]);
	} else {
		echo json_encode([
    		'status' => false,
    		'message' => 'Usaha tidak ditemukan!'
    	]);
	}
	$mysqli->close();
?>