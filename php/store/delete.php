<?php
	include '../connect.php';

	$store_id = filter_input(INPUT_POST, 'store_id', FILTER_SANITIZE_STRING);
    
	$sql = "UPDATE store SET store_delete = 1 WHERE store_id = '$store_id'";
	    
	if($mysqli->query($sql)==TRUE) {
	    echo json_encode([
    		'status' => true,
    		'data' => 'Usaha berhasil dihapus!'
    	]);
	} else {
		echo json_encode([
    		'status' => false,
    		'message' => 'Usaha gagal dihapus!'
    	]);
	}
	$mysqli->close();
?>