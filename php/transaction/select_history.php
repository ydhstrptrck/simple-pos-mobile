<?php 
	include "../connect.php";

    $date = filter_input(INPUT_POST, 'date', FILTER_SANITIZE_STRING);
    $store_id = filter_input(INPUT_POST, 'store_id', FILTER_SANITIZE_STRING);

	$sql = "SELECT * FROM transaction t INNER JOIN users u ON t.employee_id = u.user_id INNER JOIN store s ON t.store_id = s.store_id LEFT JOIN payment_type pt ON t.payment_type_id = pt.payment_type_id WHERE t.transaction_status = 'Paid' AND t.transaction_delete = 0";
	if($date != "") {
		if(strlen($date) == 10) {
			$sql .= " AND t.transaction_date = '$date'";
		} else {
			$date = explode(" - ", $date);
			$sql .= " AND t.transaction_date >= '$date[0]' AND t.transaction_date <= '$date[1]'";
		}
	}
	if($store_id != "") {
		$sql .= " AND t.store_id = '$store_id'";
	}
	$result = $mysqli->query($sql);

	$transaction = array();
	if ($result->num_rows > 0) {
		$i = 0;
		while ($obj = $result->fetch_assoc()) {
			$transaction[$i]['transaction_id'] = str_pad(addslashes(htmlentities($obj['transaction_id'])), 5, 0, STR_PAD_LEFT);
			$transaction[$i]['transaction_date'] = addslashes(htmlentities($obj['transaction_date']));
			$transaction[$i]['transaction_date_formatted'] = date('d F Y', strtotime(addslashes(htmlentities($obj['transaction_date']))));
			$transaction[$i]['transaction_time'] = addslashes(htmlentities($obj['transaction_time']));
			$transaction[$i]['transaction_status'] = addslashes(htmlentities($obj['transaction_status']));
			$transaction[$i]['transaction_customer_name'] = addslashes(htmlentities($obj['transaction_customer_name']));
			$transaction[$i]['transaction_order_number'] = addslashes(htmlentities($obj['transaction_order_number']));
			
			$transaction_order_number = "";
			if($obj['transaction_order_number'] < 10) {
				$transaction_order_number = "000" . $obj['transaction_order_number'];
			} else if($obj['transaction_order_number'] < 100) {
				$transaction_order_number = "00" . $obj['transaction_order_number'];
			} else if($obj['transaction_order_number'] < 1000) {
				$transaction_order_number = "0" . $obj['transaction_order_number'];
			} else if($obj['transaction_order_number'] < 10000) {
				$transaction_order_number = $obj['transaction_order_number'];
			}

			$transaction[$i]['transaction_invoice_number_formatted'] = date('Ymd', strtotime($obj['transaction_date'])) . $transaction_order_number;
			$transaction[$i]['transaction_total_amount'] = addslashes(htmlentities($obj['transaction_total_amount']));
			$transaction[$i]['transaction_total_discount'] = addslashes(htmlentities($obj['transaction_total_discount']));
			$transaction[$i]['transaction_grand_total'] = $obj['transaction_total_amount'] - $obj['transaction_total_discount'];
			$transaction[$i]['transaction_total_paid'] = addslashes(htmlentities($obj['transaction_total_paid']));
			$transaction[$i]['transaction_total_change'] = addslashes(htmlentities($obj['transaction_total_change']));
			$transaction[$i]['transaction_message'] = addslashes(htmlentities($obj['transaction_message']));
			$transaction[$i]['store_id'] = addslashes(htmlentities($obj['store_id']));
			$transaction[$i]['store_name'] = addslashes(htmlentities($obj['store_name']));
			$transaction[$i]['user_id'] = addslashes(htmlentities($obj['user_id']));
			$transaction[$i]['user_name'] = addslashes(htmlentities($obj['user_name']));
			$transaction[$i]['payment_type_id'] = addslashes(htmlentities($obj['payment_type_id']));
			$transaction[$i]['payment_type_name'] = addslashes(htmlentities($obj['payment_type_name']));
			$i++;
		}

		echo json_encode([
			'status' => true,
			'data' => $transaction
		]);
	} else {
		echo json_encode([
			'status' => false,
			'message' => "Tidak ada transaksi ditemukan."
		]);
	}

	$mysqli->close();
?>