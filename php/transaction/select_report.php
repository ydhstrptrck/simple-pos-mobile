<?php 
	include "../connect.php";

    $date = filter_input(INPUT_POST, 'date', FILTER_SANITIZE_STRING);
    $store_id = filter_input(INPUT_POST, 'store_id', FILTER_SANITIZE_STRING);

	$sql = "SELECT * FROM transaction t INNER JOIN users u ON t.employee_id = u.user_id INNER JOIN store s ON t.store_id = s.store_id LEFT JOIN payment_type pt ON t.payment_type_id = pt.payment_type_id WHERE t.transaction_status = 'Paid' AND t.transaction_delete = 0";
	if($date != "") {
		if(strlen($date) == 10) {
			$sql .= " AND t.transaction_date = '$date'";
		} else {
			$date = explode(" - ", $date);
			$sql .= " AND t.transaction_date >= '$date[0]' AND t.transaction_date <= '$date[1]'";
		}
	}
	if($store_id != "") {
		$sql .= " AND t.store_id = '$store_id'";
	}
	$result = $mysqli->query($sql);

	$total_income = 0;
	$total_discount = 0;
	$total_income_after_discount = 0;

	$transaction = array();
	if ($result->num_rows > 0) {
		while ($obj = $result->fetch_assoc()) {
			$total_income += $obj['transaction_total_amount'];
			$total_discount += $obj['transaction_total_discount'];
			$total_income_after_discount += ($obj['transaction_total_amount'] - $obj['transaction_total_discount']);

			$sql_transaction_detail = "SELECT * FROM transaction_detail td INNER JOIN menu m ON td.menu_id = m.menu_id WHERE td.transaction_id = '" . $obj['transaction_id'] . "'";
			$result_transaction_detail = $mysqli->query($sql_transaction_detail);
			if ($result_transaction_detail->num_rows > 0) {
				while ($obj_transaction_detail = $result_transaction_detail->fetch_assoc()) {
					if(!isset($transaction[$obj_transaction_detail['menu_id']]['menu_id'])) {
						$transaction[$obj_transaction_detail['menu_id']]['transaction_detail_count_total'] = 0;
						$transaction[$obj_transaction_detail['menu_id']]['transaction_detail_count_without_discount'] = 0;
						$transaction[$obj_transaction_detail['menu_id']]['transaction_detail_count_with_discount'] = 0;

						$transaction[$obj_transaction_detail['menu_id']]['transaction_detail_price_total'] = 0;
					}

					$transaction[$obj_transaction_detail['menu_id']]['menu_id'] = $obj_transaction_detail['menu_id'];
					$transaction[$obj_transaction_detail['menu_id']]['menu_name'] = $obj_transaction_detail['menu_name'];

					$transaction[$obj_transaction_detail['menu_id']]['transaction_detail_count_total'] += $obj_transaction_detail['transaction_detail_count'];
					if($obj_transaction_detail['transaction_detail_discount'] == 0) {
						$transaction[$obj_transaction_detail['menu_id']]['transaction_detail_count_without_discount'] += $obj_transaction_detail['transaction_detail_count'];
					} else {
						$transaction[$obj_transaction_detail['menu_id']]['transaction_detail_count_with_discount'] += $obj_transaction_detail['transaction_detail_count'];
					}

					$transaction[$obj_transaction_detail['menu_id']]['transaction_detail_price_total'] += ($obj_transaction_detail['transaction_detail_price'] - ($obj_transaction_detail['transaction_detail_price'] * $obj_transaction_detail['transaction_detail_discount'] / 100));
				}
			}
		}

		$transaction_new = array();
		$i = 0;
		foreach($transaction as $row) {
			$transaction_new[$i]['menu_id'] = $row['menu_id'];
			$transaction_new[$i]['menu_name'] = $row['menu_name'];
			$transaction_new[$i]['transaction_detail_count_total'] = $row['transaction_detail_count_total'];
			$transaction_new[$i]['transaction_detail_count_without_discount'] = $row['transaction_detail_count_without_discount'];
			$transaction_new[$i]['transaction_detail_count_with_discount'] = $row['transaction_detail_count_with_discount'];
			$transaction_new[$i]['transaction_detail_price_total'] = $row['transaction_detail_price_total'];
			$i++;
		}

		$report = array();
		$report['total_income'] = $total_income;
		$report['total_discount'] = $total_discount;
		$report['total_income_after_discount'] = $total_income_after_discount;

		echo json_encode([
			'status' => true,
			'data' => $transaction_new,
			'data_report' => $report
		]);
	} else {
		echo json_encode([
			'status' => false,
			'message' => "Tidak ada laporan ditemukan."
		]);
	}

	$mysqli->close();
?>