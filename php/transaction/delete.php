<?php
	include '../connect.php';

	$transaction_id = filter_input(INPUT_POST, 'transaction_id', FILTER_SANITIZE_STRING);
    
	$sql = "UPDATE transaction SET transaction_delete = 1 WHERE transaction_id = '$transaction_id'";
	    
	if($mysqli->query($sql)==TRUE) {
	    echo json_encode([
    		'status' => true,
    		'data' => 'Transaksi berhasil dihapus!'
    	]);
	} else {
		echo json_encode([
    		'status' => false,
    		'message' => 'Transaksi gagal dihapus!'
    	]);
	}
	$mysqli->close();
?>