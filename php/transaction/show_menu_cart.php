<?php 
	include "../connect.php";

    $menu_id = filter_input(INPUT_POST, 'menu_id', FILTER_SANITIZE_STRING);
    $store_id = filter_input(INPUT_POST, 'store_id', FILTER_SANITIZE_STRING);

	$sql = "SELECT * FROM transaction t INNER JOIN transaction_detail td ON t.transaction_id = td.transaction_id WHERE td.menu_id = '$menu_id' AND t.transaction_status = 'Pending' AND t.store_id = '$store_id'";
	$result = $mysqli->query($sql);

	$menu_cart = array();
	$i = 0;
	if ($result->num_rows > 0) {
		while ($obj = $result->fetch_assoc()) {
			$menu_cart[$i]['transaction_detail_count'] = addslashes(htmlentities($obj['transaction_detail_count']));
		}
	} else {
		$menu_cart[$i]['transaction_detail_count'] = 0;
	}

	echo json_encode([
		'status' => true,
		'data' => $menu_cart
	]);

	$mysqli->close();
?>