<?php 
	include "../connect.php";

    $transaction_id = filter_input(INPUT_POST, 'transaction_id', FILTER_SANITIZE_STRING);
    $store_id = filter_input(INPUT_POST, 'store_id', FILTER_SANITIZE_STRING);

	$sql = "SELECT * FROM transaction t INNER JOIN transaction_detail td ON t.transaction_id = td.transaction_id INNER JOIN menu m ON td.menu_id = m.menu_id INNER JOIN users u ON t.employee_id = u.user_id LEFT JOIN payment_type pt ON t.payment_type_id = pt.payment_type_id INNER JOIN store s ON t.store_id = s.store_id WHERE t.transaction_delete = 0 AND";
	if($transaction_id != 0) {
		$sql .= " t.transaction_id = $transaction_id";
	} else {
		$sql .= " t.transaction_status = 'Pending' AND t.store_id = '$store_id'";
	}
	$result = $mysqli->query($sql);

	$transaction = array();
	if ($result->num_rows > 0) {
		$i = 0;
		while ($obj = $result->fetch_assoc()) {
			$transaction[$i]['transaction_id'] = addslashes(htmlentities($obj['transaction_id']));
			$transaction[$i]['transaction_date'] = addslashes(htmlentities($obj['transaction_date']));
			$transaction[$i]['transaction_date_formatted'] = date('d F Y', strtotime($obj['transaction_date']));
			$transaction[$i]['transaction_time'] = addslashes(htmlentities($obj['transaction_time']));
			$transaction[$i]['transaction_status'] = addslashes(htmlentities($obj['transaction_status']));
			$transaction[$i]['transaction_customer_name'] = addslashes(htmlentities($obj['transaction_customer_name']));
			$transaction[$i]['transaction_order_number'] = addslashes(htmlentities($obj['transaction_order_number']));
			$transaction[$i]['transaction_order_number_formatted'] = str_pad($obj['transaction_order_number'], 5, 0, STR_PAD_LEFT);
			
			$transaction_order_number = "";
			if($obj['transaction_order_number'] < 10) {
				$transaction_order_number = "000" . $obj['transaction_order_number'];
			} else if($obj['transaction_order_number'] < 100) {
				$transaction_order_number = "00" . $obj['transaction_order_number'];
			} else if($obj['transaction_order_number'] < 1000) {
				$transaction_order_number = "0" . $obj['transaction_order_number'];
			} else if($obj['transaction_order_number'] < 10000) {
				$transaction_order_number = $obj['transaction_order_number'];
			}

			$transaction[$i]['transaction_invoice_number_formatted'] = date('Ymd', strtotime($obj['transaction_date'])) . $transaction_order_number;
			$transaction[$i]['transaction_total_amount'] = addslashes(htmlentities($obj['transaction_total_amount']));
			$transaction[$i]['transaction_total_discount'] = addslashes(htmlentities($obj['transaction_total_discount']));
			$transaction[$i]['transaction_total_paid'] = addslashes(htmlentities($obj['transaction_total_paid']));
			$transaction[$i]['transaction_total_change'] = addslashes(htmlentities($obj['transaction_total_change']));
			$transaction_message = $obj['transaction_message'];
			if($obj['transaction_message'] == "" || $obj['transaction_message'] == null) {
				$transaction_message = "----";
			}
			$transaction[$i]['transaction_message'] = $transaction_message;
			$transaction[$i]['store_id'] = addslashes(htmlentities($obj['store_id']));
			$transaction[$i]['user_id'] = addslashes(htmlentities($obj['user_id']));
			$transaction[$i]['user_name'] = addslashes(htmlentities($obj['user_name']));
			$transaction[$i]['payment_type_id'] = addslashes(htmlentities($obj['payment_type_id']));
			$transaction[$i]['payment_type_name'] = addslashes(htmlentities($obj['payment_type_name']));
			$transaction[$i]['transaction_detail_id'] = addslashes(htmlentities($obj['transaction_detail_id']));
			$transaction[$i]['transaction_detail_count'] = addslashes(htmlentities($obj['transaction_detail_count']));
			$transaction[$i]['transaction_detail_price'] = addslashes(htmlentities($obj['transaction_detail_price']));
			$transaction[$i]['transaction_detail_discount'] = addslashes(htmlentities($obj['transaction_detail_discount']));
			$transaction[$i]['menu_id'] = addslashes(htmlentities($obj['menu_id']));
			$transaction[$i]['menu_name'] = addslashes(htmlentities($obj['menu_name']));
			$transaction[$i]['store_id'] = addslashes(htmlentities($obj['store_id']));
			$transaction[$i]['store_name'] = addslashes(htmlentities($obj['store_name']));
			$transaction[$i]['store_address'] = addslashes(htmlentities($obj['store_address']));
			$i++;
		}
	}

	echo json_encode([
		'status' => true,
		'data' => $transaction
	]);

	$mysqli->close();
?>