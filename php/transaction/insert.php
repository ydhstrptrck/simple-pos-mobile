<?php 
	include "../connect.php";

	$menu_id = filter_input(INPUT_POST, 'menu_id', FILTER_SANITIZE_STRING);
    $transaction_detail_count = filter_input(INPUT_POST, 'transaction_detail_count', FILTER_SANITIZE_STRING);
    $user_id = filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_STRING);
    $store_id = filter_input(INPUT_POST, 'store_id', FILTER_SANITIZE_STRING);

    $transaction_id = 0;

    // insert transaction
	$sql = "SELECT * FROM transaction WHERE transaction_status = 'Pending' AND store_id = $store_id";
	$result = $mysqli->query($sql);

	if ($result->num_rows > 0) {
		while ($obj = $result->fetch_assoc()) {
			$transaction_id = addslashes(htmlentities($obj['transaction_id']));
	    }
	} else {
		$transaction_order_number = 1;
		$transaction_date = date('Y-m-d');

		$sql = "SELECT * FROM transaction WHERE transaction_date = '$transaction_date' AND store_id = '$store_id' ORDER BY transaction_order_number DESC LIMIT 1";
		$result = $mysqli->query($sql);
		if ($result->num_rows > 0) {
			while ($obj = $result->fetch_assoc()) {
				$transaction_order_number = $obj['transaction_order_number'] + 1;
		    }
		}

		$transaction_time = date('H:i:s');
		$transaction_status = "Pending";
		$sql = "INSERT INTO transaction(transaction_date, transaction_time, transaction_status, transaction_order_number, transaction_delete, employee_id, store_id) VALUES('" .
            $transaction_date . "','" .
            $transaction_time . "','" .
            $transaction_status . "','" .
            $transaction_order_number . "','" .
            "0" . "','" .
            $user_id . "','" .
            $store_id .
            "')";
        $mysqli->query($sql);
        $transaction_id = $mysqli->insert_id;
	}

	// get detail menu
	$transaction_detail_price = 0;
	$transaction_detail_discount = 0;
	
	$sql = "SELECT * FROM menu WHERE menu_id = '$menu_id'";
	$result = $mysqli->query($sql);
	
	if ($result->num_rows > 0) {
		while ($obj = $result->fetch_assoc()) {
			$transaction_detail_price = $obj['menu_sell_price'];
			$transaction_detail_discount = $obj['menu_discount'];
		}
	}

	if($transaction_detail_count == 0 || $transaction_detail_count == 99) {
		// delete transaction detail
		$sql = "DELETE FROM transaction_detail WHERE transaction_id = $transaction_id AND menu_id = $menu_id";
		$mysqli->query($sql);
	} else {
		// insert transaction detail
		$sql = "SELECT * FROM transaction_detail WHERE transaction_id = $transaction_id AND menu_id = $menu_id";
		$result = $mysqli->query($sql);

		if ($result->num_rows > 0) {
			$sql = "UPDATE transaction_detail SET transaction_detail_count = $transaction_detail_count WHERE transaction_id = $transaction_id AND menu_id = $menu_id";
			$mysqli->query($sql);
		} else {
			$sql = "INSERT INTO transaction_detail(transaction_detail_count, transaction_detail_price, transaction_detail_discount, menu_id, transaction_id) VALUES('" .
	            $transaction_detail_count . "','" .
	            $transaction_detail_price . "','" .
	            $transaction_detail_discount . "','" .
	            $menu_id . "','" .
	            $transaction_id .
	            "')";
	        $mysqli->query($sql);
		}
	}

	// calculate transaction total amount and discount
	$transaction_total_amount = 0;
	$transaction_total_discount = 0;

	$sql = "SELECT * FROM transaction t INNER JOIN transaction_detail td ON t.transaction_id = td.transaction_id WHERE t.transaction_id = $transaction_id";
	$result = $mysqli->query($sql);

	if ($result->num_rows > 0) {
		while ($obj = $result->fetch_assoc()) {
			$transaction_total_amount += ($obj['transaction_detail_count'] * $obj['transaction_detail_price']);
			$transaction_total_discount += (($obj['transaction_detail_price'] * $obj['transaction_detail_discount'] / 100) * $obj['transaction_detail_count']);
		}

		$sql = "UPDATE transaction SET transaction_total_amount = $transaction_total_amount, transaction_total_discount = $transaction_total_discount WHERE transaction_id = $transaction_id";
		$mysqli->query($sql);
	}

	$mysqli->close();
?>