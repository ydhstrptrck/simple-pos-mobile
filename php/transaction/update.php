<?php 
	include "../connect.php";

	$transaction_id = filter_input(INPUT_POST, 'transaction_id', FILTER_SANITIZE_STRING);
    $payment_type_id = filter_input(INPUT_POST, 'payment_type_id', FILTER_SANITIZE_STRING);
    $transaction_customer_name = filter_input(INPUT_POST, 'transaction_customer_name', FILTER_SANITIZE_STRING);
    $transaction_message = filter_input(INPUT_POST, 'transaction_message', FILTER_SANITIZE_STRING);
    $transaction_total_paid = filter_input(INPUT_POST, 'transaction_total_paid', FILTER_SANITIZE_STRING);
    $transaction_total_change = filter_input(INPUT_POST, 'transaction_total_change', FILTER_SANITIZE_STRING);
    $user_id = filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_STRING);
    $store_id = filter_input(INPUT_POST, 'store_id', FILTER_SANITIZE_STRING);

	$sql = "SELECT * FROM transaction WHERE transaction_status = 'Pending' AND store_id = $store_id";
	$result = $mysqli->query($sql);

	if ($result->num_rows > 0) {
		while ($obj = $result->fetch_assoc()) {
			$transaction_id = addslashes(htmlentities($obj['transaction_id']));
	    }
	}

	$sql = "UPDATE transaction SET payment_type_id = $payment_type_id, transaction_customer_name = '$transaction_customer_name', transaction_message = '$transaction_message', transaction_total_paid = '$transaction_total_paid', transaction_total_change = '$transaction_total_change', employee_id = '$user_id', transaction_status = 'Paid' WHERE transaction_id = $transaction_id";
	$mysqli->query($sql);

	echo json_encode([
		'status' => true,
		'transaction_id' => $transaction_id
	]);

	$mysqli->close();
?>