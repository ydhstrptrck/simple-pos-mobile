<?php 
	include "../connect.php";
    
    $user_id = filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_STRING);
    
	$sql = "SELECT * FROM users WHERE user_id = '$user_id'";
	$result = $mysqli->query($sql);
	
	if ($result->num_rows > 0) {
		$user = array();
		$i = 0;
		while ($obj = $result->fetch_assoc()) {
			$user[$i]['user_id'] = addslashes(htmlentities($obj['user_id']));
			$user[$i]['username'] = addslashes(htmlentities($obj['username']));
			$user[$i]['user_name'] = addslashes(htmlentities($obj['user_name']));
			$user[$i]['user_phone'] = addslashes(htmlentities($obj['user_phone']));
			$user[$i]['email'] = addslashes(htmlentities($obj['email']));
			$user[$i]['user_role'] = addslashes(htmlentities($obj['user_role']));
			$user[$i]['user_delete'] = addslashes(htmlentities($obj['user_delete']));
			$user[$i]['store_id'] = addslashes(htmlentities($obj['store_id']));
			$i++;
	    }
		echo json_encode([
    		'status' => true,
    		'data' => $user
    	]);
	} else {
		echo json_encode([
    		'status' => false,
    		'message' => 'Karyawan tidak ditemukan!'
    	]);
	}
	$mysqli->close();
?>