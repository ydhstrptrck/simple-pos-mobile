<?php
	include '../connect.php';

	$user_id = filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_STRING);
    
	$sql = "UPDATE users SET user_delete = 1 WHERE user_id = '$user_id'";
	    
	if($mysqli->query($sql)==TRUE) {
	    echo json_encode([
    		'status' => true,
    		'data' => 'Karyawan berhasil dihapus!'
    	]);
	} else {
		echo json_encode([
    		'status' => false,
    		'message' => 'Karyawan gagal dihapus!'
    	]);
	}
	$mysqli->close();
?>