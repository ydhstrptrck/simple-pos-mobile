<?php
	include "../bcrypt.php";
    include "../connect.php";
    
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $user_name = filter_input(INPUT_POST, 'user_name', FILTER_SANITIZE_STRING);
    $user_phone = filter_input(INPUT_POST, 'user_phone', FILTER_SANITIZE_STRING);
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    $user_role = filter_input(INPUT_POST, 'user_role', FILTER_SANITIZE_STRING);
    $user_delete = 0;
    $store_id = filter_input(INPUT_POST, 'store_id', FILTER_SANITIZE_STRING);

    $bcrypt = new Bcrypt(16);
    $password = $bcrypt->hash($password);
    
    $sql = "";
    if($store_id == 0) {
        $sql = "INSERT INTO users(username, user_name, user_phone, email, password, user_role, user_delete) VALUES('" .
            $username . "','" .
            $user_name . "','" .
            $user_phone . "','" .
            $email . "','" .
            $password . "','" .
            $user_role . "','" .
            $user_delete .
            "')";
    } else {
        $sql = "INSERT INTO users(username, user_name, user_phone, email, password, user_role, user_delete, store_id) VALUES('" .
            $username . "','" .
            $user_name . "','" .
            $user_phone . "','" .
            $email . "','" .
            $password . "','" .
            $user_role . "','" .
            $user_delete . "','" .
            $store_id .
            "')";
    }
	    
	if($mysqli->query($sql) == TRUE) {
	    echo json_encode([
    		'status' => true,
    		'data' => 'Karyawan baru berhasil disimpan!'
    	]);
	} else {
		echo json_encode([
    		'status' => false,
    		'message' => 'Karyawan baru gagal disimpan!'
    	]);
	}

	$mysqli->close();
?>