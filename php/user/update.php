<?php
    include "../bcrypt.php";
    include "../connect.php";
    
    $user_id = filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_STRING);
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $user_name = filter_input(INPUT_POST, 'user_name', FILTER_SANITIZE_STRING);
    $user_phone = filter_input(INPUT_POST, 'user_phone', FILTER_SANITIZE_STRING);
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    $store_id = filter_input(INPUT_POST, 'store_id', FILTER_SANITIZE_STRING);

    $sql = "";
    if($password == "") {
        $sql = "UPDATE users SET username = '$username', user_name = '$user_name', user_phone = '$user_phone', email = '$email'";   
    } else {
        $bcrypt = new Bcrypt(16);
        $password = $bcrypt->hash($password);
        
        $sql = "UPDATE users SET username = '$username', user_name = '$user_name', user_phone = '$user_phone', email = '$email', password = '$password'";   
    }
    if($store_id != 0) {
        $sql .= ", store_id = '$store_id'";
    } else {
        $sql .= ", store_id = NULL";
    }
    $sql .= " WHERE user_id = '$user_id'";
        
    if($mysqli->query($sql)==TRUE) {
        echo json_encode([
            'status' => true,
            'data' => 'Karyawan berhasil diubah!'
        ]);
    } else {
        echo json_encode([
            'status' => false,
            'message' => 'Karyawan gagal diubah!'
        ]);
    }
    
    $mysqli->close();
?>