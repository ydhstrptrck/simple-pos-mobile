<?php 
	include "../connect.php";
    
	$sql = "SELECT * FROM users u LEFT JOIN store s ON u.store_id = s.store_id WHERE user_delete = 0";
	if(isset($_GET['keyword'])) {
		$sql .= " AND user_name LIKE '%" . $_GET['keyword'] . "%'";
	}
	if(isset($_GET['store_id'])) {
		if($_GET['store_id'] != "") {
			$sql .= " AND u.store_id = '" . $_GET['store_id'] . "'";
		}
	}

	$result = $mysqli->query($sql);

	if ($result->num_rows > 0) {
		$user = array();
		$i = 0;
		while ($obj = $result->fetch_assoc()) {
			$user[$i]['user_id'] = addslashes(htmlentities($obj['user_id']));
			$user[$i]['username'] = addslashes(htmlentities($obj['username']));
			$user[$i]['user_name'] = addslashes(htmlentities($obj['user_name']));
			$user[$i]['user_phone'] = addslashes(htmlentities($obj['user_phone']));
			$user[$i]['email'] = addslashes(htmlentities($obj['email']));
			if($obj['email'] == "") {
				$user[$i]['email'] = "<font style='color: red;'>Tidak ada email</font>";
			}
			$user[$i]['user_role'] = addslashes(htmlentities($obj['user_role']));
			$user[$i]['store_id'] = addslashes(htmlentities($obj['store_id']));
			$store_name = $obj['store_name'];
			if($store_name == "" && $obj['user_role'] != "Admin") {
				$store_name = "<font style='color: red;'>Tidak ada</font>";
			} else if($obj['user_role'] == "Admin") {
				$store_name = "";
			}
			$user[$i]['store_name'] = $store_name;
			$user[$i]['store_address'] = addslashes(htmlentities($obj['store_address']));
			$i++;
	    }
		echo json_encode([
    		'status' => true,
    		'data' => $user
    	]);
	} else {
		echo json_encode([
    		'status' => false,
    		'message' => 'Tidak ada karyawan ditemukan!'
    	]);
	}

	$mysqli->close();
?>