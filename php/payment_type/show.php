<?php 
	include "../connect.php";
    
    $payment_type_id = filter_input(INPUT_POST, 'payment_type_id', FILTER_SANITIZE_STRING);
    
	$sql = "SELECT * FROM payment_type WHERE payment_type_id = '$payment_type_id'";
	$result = $mysqli->query($sql);
	
	if ($result->num_rows > 0) {
		$payment_type = array();
		$i = 0;
		while ($obj = $result->fetch_assoc()) {
			$payment_type[$i]['payment_type_id'] = addslashes(htmlentities($obj['payment_type_id']));
			$payment_type[$i]['payment_type_name'] = addslashes(htmlentities($obj['payment_type_name']));
			$i++;
	    }
		echo json_encode([
    		'status' => true,
    		'data' => $payment_type
    	]);
	} else {
		echo json_encode([
    		'status' => false,
    		'message' => 'Jenis pembayaran tidak ditemukan!'
    	]);
	}
	$mysqli->close();
?>