<?php
    include "../bcrypt.php";
    include "../connect.php";
    
    $payment_type_id = filter_input(INPUT_POST, 'payment_type_id', FILTER_SANITIZE_STRING);
    $payment_type_name = filter_input(INPUT_POST, 'payment_type_name', FILTER_SANITIZE_STRING);

    $sql = "UPDATE payment_type SET payment_type_name = '$payment_type_name' WHERE payment_type_id = '$payment_type_id'";   
	    
	if($mysqli->query($sql)==TRUE) {
	    echo json_encode([
    		'status' => true,
    		'data' => 'Jenis pembayaran berhasil diubah!'
    	]);
	} else {
		echo json_encode([
    		'status' => false,
    		'message' => 'Jenis pembayaran gagal diubah!'
    	]);
	}
    
	$mysqli->close();
?>