<?php 
	include "../connect.php";
    
	$sql = "SELECT * FROM payment_type WHERE payment_type_delete = 0";
	if(isset($_GET['keyword'])) {
		$sql .= " AND payment_type_name LIKE '%" . $_GET['keyword'] . "%'";
	}

	$result = $mysqli->query($sql);

	if ($result->num_rows > 0) {
		$payment_type = array();
		$i = 0;
		while ($obj = $result->fetch_assoc()) {
			$payment_type[$i]['payment_type_id'] = addslashes(htmlentities($obj['payment_type_id']));
			$payment_type[$i]['payment_type_name'] = addslashes(htmlentities($obj['payment_type_name']));
			$i++;
	    }
		echo json_encode([
    		'status' => true,
    		'data' => $payment_type
    	]);
	} else {
		echo json_encode([
    		'status' => false,
    		'message' => 'Tidak ada jenis pembayaran ditemukan!'
    	]);
	}

	$mysqli->close();
?>