<?php
	include "../bcrypt.php";
    include "../connect.php";
    
    $menu_code = filter_input(INPUT_POST, 'menu_code', FILTER_SANITIZE_STRING);
    $menu_name = filter_input(INPUT_POST, 'menu_name', FILTER_SANITIZE_STRING);
    $menu_sell_price = filter_input(INPUT_POST, 'menu_sell_price', FILTER_SANITIZE_STRING);
    $menu_discount = filter_input(INPUT_POST, 'menu_discount', FILTER_SANITIZE_STRING);
    $menu_delete = 0;
    $store_id = filter_input(INPUT_POST, 'store_id', FILTER_SANITIZE_STRING);
    
    $sql = "INSERT INTO menu(menu_code, menu_name, menu_sell_price, menu_discount, menu_delete, store_id) VALUES('" .
    $menu_code . "','" .
    $menu_name . "','" .
    $menu_sell_price . "','" .
    $menu_discount . "','" .
    $menu_delete . "','" .
    $store_id .
    "')";
	    
	if($mysqli->query($sql) == TRUE) {
	    echo json_encode([
    		'status' => true,
    		'data' => 'Menu baru berhasil disimpan!'
    	]);
	} else {
		echo json_encode([
    		'status' => false,
    		'message' => 'Menu baru gagal disimpan!'
    	]);
	}

	$mysqli->close();
?>