<?php
	include '../connect.php';

	$menu_id = filter_input(INPUT_POST, 'menu_id', FILTER_SANITIZE_STRING);
    
	$sql = "UPDATE menu SET menu_delete = 1 WHERE menu_id = '$menu_id'";
	    
	if($mysqli->query($sql)==TRUE) {
	    echo json_encode([
    		'status' => true,
    		'data' => 'Menu berhasil dihapus!'
    	]);
	} else {
		echo json_encode([
    		'status' => false,
    		'message' => 'Menu gagal dihapus!'
    	]);
	}
	$mysqli->close();
?>