<?php 
	// include "../connect.php";
    
	// $sql = "SELECT * FROM menu m INNER JOIN store s ON m.store_id = s.store_id WHERE menu_delete = 0 AND store_delete = 0";
	// if(isset($_GET['keyword'])) {
	// 	$sql .= " AND menu_name LIKE '%" . $_GET['keyword'] . "%'";
	// }
	// if(isset($_GET['store_id'])) {
	// 	if($_GET['store_id'] != "") {
	// 		$sql .= " AND m.store_id = '" . $_GET['store_id'] . "'";
	// 	}
	// }
	// $sql .= " ORDER BY m.menu_code ASC";

	// $result = $mysqli->query($sql);

	// if ($result->num_rows > 0) {
	// 	$menu = array();
	// 	$i = 0;
	// 	while ($obj = $result->fetch_assoc()) {
	// 		$menu[$i]['menu_id'] = addslashes(htmlentities($obj['menu_id']));
	// 		$menu[$i]['menu_code'] = addslashes(htmlentities($obj['menu_code']));
	// 		$menu[$i]['menu_name'] = addslashes(htmlentities($obj['menu_name']));
	// 		$menu[$i]['menu_sell_price'] = addslashes(htmlentities($obj['menu_sell_price']));
	// 		$menu[$i]['menu_discount'] = addslashes(htmlentities($obj['menu_discount']));
	// 		$menu[$i]['store_id'] = addslashes(htmlentities($obj['store_id']));
	// 		$menu[$i]['store_name'] = addslashes(htmlentities($obj['store_name']));
	// 		$menu[$i]['store_address'] = addslashes(htmlentities($obj['store_address']));
	// 		$i++;
	//     }
	// 	echo json_encode([
 //    		'status' => true,
 //    		'data' => $menu
 //    	]);
	// } else {
	// 	echo json_encode([
 //    		'status' => false,
 //    		'message' => 'Tidak ada menu ditemukan!'
 //    	]);
	// }

	// $mysqli->close();

	include "../connect.php";
    
	$sql = "SELECT * FROM menu m INNER JOIN store s ON m.store_id = s.store_id WHERE menu_delete = 0 AND store_delete = 0";
	if(isset($_GET['keyword'])) {
		$sql .= " AND (menu_name LIKE '%" . $_GET['keyword'] . "%' OR store_name LIKE '%" . $_GET['keyword'] . "%')";
	}
    if(isset($_GET['store_id']) && isset($_GET['admin'])) {
        if($_GET['store_id'] != "") {
                $sql .= " AND m.store_id = '" . $_GET['store_id'] . "'";
        }
    }
	$sql .= " ORDER BY s.store_name, m.menu_name ASC";

	$result = $mysqli->query($sql);

	if ($result->num_rows > 0) {
		$menu = array();
		$i = 0;
		while ($obj = $result->fetch_assoc()) {
			$menu[$i]['menu_id'] = addslashes(htmlentities($obj['menu_id']));
			$menu[$i]['menu_code'] = addslashes(htmlentities($obj['menu_code']));
			$menu[$i]['menu_name'] = "[" . addslashes(htmlentities($obj['store_name'])) . "] " . addslashes(htmlentities($obj['menu_name']));
			$menu[$i]['menu_sell_price'] = addslashes(htmlentities($obj['menu_sell_price']));
			$menu[$i]['menu_discount'] = addslashes(htmlentities($obj['menu_discount']));
			$menu[$i]['store_id'] = addslashes(htmlentities($obj['store_id']));
			$menu[$i]['store_name'] = addslashes(htmlentities($obj['store_name']));
			$menu[$i]['store_address'] = addslashes(htmlentities($obj['store_address']));
			$i++;
	    }
		echo json_encode([
    		'status' => true,
    		'data' => $menu
    	]);
	} else {
		echo json_encode([
    		'status' => false,
    		'message' => 'Tidak ada menu ditemukan!'
    	]);
	}

	$mysqli->close();
?>