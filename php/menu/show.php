<?php 
	include "../connect.php";
    
    $menu_id = filter_input(INPUT_POST, 'menu_id', FILTER_SANITIZE_STRING);
    
	$sql = "SELECT * FROM menu WHERE menu_id = '$menu_id'";
	$result = $mysqli->query($sql);
	
	if ($result->num_rows > 0) {
		$menu = array();
		$i = 0;
		while ($obj = $result->fetch_assoc()) {
			$menu[$i]['menu_id'] = addslashes(htmlentities($obj['menu_id']));
			$menu[$i]['menu_code'] = addslashes(htmlentities($obj['menu_code']));
			$menu[$i]['menu_name'] = addslashes(htmlentities($obj['menu_name']));
			$menu[$i]['menu_sell_price'] = addslashes(htmlentities($obj['menu_sell_price']));
			$menu[$i]['menu_discount'] = addslashes(htmlentities($obj['menu_discount']));
			$menu[$i]['store_id'] = addslashes(htmlentities($obj['store_id']));
			$i++;
	    }
		echo json_encode([
    		'status' => true,
    		'data' => $menu
    	]);
	} else {
		echo json_encode([
    		'status' => false,
    		'message' => 'Menu tidak ditemukan!'
    	]);
	}
	$mysqli->close();
?>