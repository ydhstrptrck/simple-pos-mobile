function load_menu_create(e, page) {
  // loadingData();

  app.request({
    method: "GET",
    url: urlxampp + "store/select.php", 
    data: {  
      
    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        for(var i = 0; i < x.length; i++) {
          $$('#store_id_create_menu').append(`
            <option value="` + x[i]['store_id'] + `">` + x[i]['store_name'] + `</option>
          `);
        }
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });
  
  $$('#btn_create_menu').on('click', function() {
    var menu_code = $$('#menu_code_create_menu').val();
    var menu_name = $$('#menu_name_create_menu').val();
    var menu_sell_price = $$('#menu_sell_price_create_menu').val();
    var menu_discount = $$('#menu_discount_create_menu').val();
    var store_id = $$('#store_id_create_menu').val();

    if(menu_code == "") {
      app.dialog.alert('Kode menu wajib diisi!');
    } else if(menu_name == "") {
      app.dialog.alert('Nama menu wajib diisi!');
    } else if(menu_sell_price == "") {
      app.dialog.alert('Harga jual menu wajib diisi!');
    } else if(parseInt(menu_sell_price) < 0) {
      app.dialog.alert('Harga jual menu tidak boleh lebih dari kurang dari 0!');
    } else if(menu_discount == "") {
      app.dialog.alert('Diskon menu wajib diisi!');
    } else if(parseInt(menu_discount) < 0 || parseInt(menu_discount) > 100) {
      app.dialog.alert('Diskon menu tidak boleh lebih dari kurang dari 0 atau lebih dari 100!');
    } else if(parseInt(store_id) === 0) {
      app.dialog.alert('Usaha wajib dipilih!');
    } else {
      app.request({
        method: "POST",
        url: urlxampp + "menu/insert.php", 
        data: {  
          menu_code : menu_code,
          menu_name : menu_name,
          menu_sell_price : menu_sell_price,
          menu_discount : menu_discount,
          store_id : store_id
        },
        success: function(data) {
          var obj = JSON.parse(data);
          determinateLoading = false;
          app.dialog.close();
          if(obj['status'] == true) {
            app.dialog.alert(obj['data'], 'Notifikasi', function(){
              page.router.navigate('/menu/index');
            });
          } else {
            app.dialog.alert(obj['message']);
          }
        },
        error: function(data) {
          determinateLoading = false;
          app.dialog.close();
          var toastBottom = app.toast.create({
            text: error_connection,
            closeTimeout: 2000,
          });
          toastBottom.open();
          page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
        }
      });
    }
  });
}