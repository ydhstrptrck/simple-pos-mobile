function load_menu_index(e, page) {
  loadingData();
  
  app.request({
    method: "GET",
    url: urlxampp + "store/select.php", 
    data: {  
      
    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        for(var i = 0; i < x.length; i++) {
          $$('#choose_store_menu_index').append(`
            <option value="` + x[i]['store_id'] + `">` + x[i]['store_name'] + `</option>
          `);
        }
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });

  app.request({
    method: "GET",
    url: urlxampp + "menu/select.php", 
    data: {  

    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        load_list_menu(x);
      } else {
        $$('#list_menu').html(`
          <div class="row">
            <div class="col">
              <div class="container-image">
                <img src="img/list-menu-empty.png" style="width: 100%; height: 65px;">
                <div class="center-of-image-list-menu" style="color: black; text-align: left;">
                  ` + obj['message'] + ` 
                </div>
              </div>
            </div>
          </div>
        `);
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });

  $$('#txt_search_menu').on('keyup', function() {
    app.request({
      method: "GET",
      url: urlxampp + "menu/select.php", 
      data:{ 
        keyword : $$('#txt_search_menu').val(),
        store_id : $$('#choose_store_menu_index').val(),
        admin : 'yes'
      },
      success: function(data) {
        var obj = JSON.parse(data);
        determinateLoading = false;
        app.dialog.close();
        if(obj['status'] == true) {
          var x = obj['data'];
          load_list_menu(x);
        } else {
          $$('#list_menu').html(`
            <div class="row">
              <div class="col">
                <div class="container-image">
                  <img src="img/list-menu-empty.png" style="width: 100%; height: 65px;">
                  <div class="center-of-image-list-menu" style="color: black; text-align: left;">
                    ` + obj['message'] + ` 
                  </div>
                </div>
              </div>
            </div>
          `);
        }
      },
      error: function(data) {
        determinateLoading = false;
        app.dialog.close();
        var toastBottom = app.toast.create({
          text: error_connection,
          closeTimeout: 2000,
        });
        toastBottom.open();
        page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
      }
    });
  });

  $$('#choose_store_menu_index').on('change', function() {
    app.request({
      method: "GET",
      url: urlxampp + "menu/select.php", 
      data:{ 
        keyword : $$('#txt_search_menu').val(),
        store_id : $$('#choose_store_menu_index').val(),
        admin : 'yes'
      },
      success: function(data) {
        var obj = JSON.parse(data);
        determinateLoading = false;
        app.dialog.close();
        if(obj['status'] == true) {
          var x = obj['data'];
          load_list_menu(x);
        } else {
          $$('#list_menu').html(`
            <div class="row">
              <div class="col">
                <div class="container-image">
                  <img src="img/list-menu-empty.png" style="width: 100%; height: 65px;">
                  <div class="center-of-image-list-menu" style="color: black; text-align: left;">
                    ` + obj['message'] + ` 
                  </div>
                </div>
              </div>
            </div>
          `);
        }
      },
      error: function(data) {
        determinateLoading = false;
        app.dialog.close();
        var toastBottom = app.toast.create({
          text: error_connection,
          closeTimeout: 2000,
        });
        toastBottom.open();
        page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
      }
    });
  });
}

function load_list_menu(x) {
  $$('#list_menu').html('');
  for(var i = 0; i < x.length; i++) {
    $$('#list_menu').append(`
      <div class="row">
        <div class="col">
          <div class="container-image">
            <img src="img/list-menu.png" style="width: 100%; height: 150px;">
            <div class="top-left-of-image-list-menu" style="color: black; text-align: left;">
              <span style="font-size: 16px;"><b>` + x[i]['menu_name'] + `</b></span>` +
              ` <br> ` + x[i]['menu_code'] + 
              ` <br> ` + formatRupiah(x[i]['menu_sell_price']) + 
              ` <br> Diskon : ` + x[i]['menu_discount'] + `%` +
              ` <br> ` + x[i]['store_name'] + 
            `</div>
            <div class="bottom-right-of-image-list-menu">
              <a class="link" href="/menu/edit/` + x[i]['menu_id'] + `">
                <img src="img/icon-edit.png" style="width: 80%;">
              </a> <br>
              <a class="link delete_menu" data-id="` + x[i]['menu_id'] + `">
                <img src="img/icon-delete.png" style="width: 80%;">
              </a>
            </div>
          </div>
        </div>
      </div>
    `);
  }

  $$('.delete_menu').on('click', function () {
    var id = $$(this).data('id');
    app.dialog.confirm("Apakah Anda yakin untuk menghapus menu ini?", 'Konfirmasi', function() {
      loadingData();
      app.request({
        method:"POST",
        url: urlxampp + "menu/delete.php",
        data: {
          menu_id : id
        },
        success:function(data){
          var obj = JSON.parse(data);
          determinateLoading = false;
          app.dialog.close();
          if(obj['status'] == true) {
            var x = obj['data'];
            app.dialog.alert(x, 'Notifikasi', function() {
              mainView.router.refreshPage();
            });
          } else {
            app.dialog.alert(obj['message']);
          }
        },
        error:function(data){
          determinateLoading = false;
          app.dialog.close();
          var toastBottom = app.toast.create({
            text: ERRNC,
            closeTimeout: 2000,
          });
          toastBottom.open();
          page.router.navigate('/user_home/',{ animate:false, reloadAll:true, force: true, ignoreCache: true });
        }
      });
    });
  });
}