function load_store_create(e, page) {
  $$('#btn_create_store').on('click', function() {
    var store_name = $$('#store_name_create_store').val();
    var store_address = $$('#store_address_create_store').val();

    if(store_name == "") {
      app.dialog.alert('Nama usaha wajib diisi!');
    } else if(store_address == "") {
      app.dialog.alert('Alamat usaha wajib diisi!');
    } else {
      app.request({
        method: "POST",
        url: urlxampp + "store/insert.php", 
        data: {  
          store_name : store_name,
          store_address : store_address
        },
        success: function(data) {
          var obj = JSON.parse(data);
          determinateLoading = false;
          app.dialog.close();
          if(obj['status'] == true) {
            app.dialog.alert(obj['data'], 'Notifikasi', function(){
              page.router.navigate('/store/index');
            });
          } else {
            app.dialog.alert(obj['message']);
          }
        },
        error: function(data) {
          determinateLoading = false;
          app.dialog.close();
          var toastBottom = app.toast.create({
            text: error_connection,
            closeTimeout: 2000,
          });
          toastBottom.open();
          page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
        }
      });
    }
  });
}