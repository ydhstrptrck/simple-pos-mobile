function load_transaction_menu_detail (e, page) {
  var menu_id = page.router.currentRoute.params.menu_id;
  // loadingData();
  
  app.request({
    method: "POST",
    url: urlxampp + "menu/show.php", 
    data: {  
      menu_id : menu_id
    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        var menu_sell_price = parseInt(x[0]['menu_sell_price']);
        var menu_discount = parseInt(x[0]['menu_discount']);
        var menu_sell_price_after_discount = "";
        if(menu_discount > 0) {
          menu_sell_price_after_discount = formatRupiah((menu_sell_price - (menu_sell_price * menu_discount / 100)).toString());
          menu_sell_price = "<strike>" + formatRupiah(menu_sell_price.toString()) + "</strike>";
        } else {
          menu_sell_price = formatRupiah(menu_sell_price.toString());
        }

        $$('#menu_name_transaction_menu_detail').html(x[0]['menu_name']);
        $$('#menu_sell_price_transaction_menu_detail').html(menu_sell_price + " " + menu_sell_price_after_discount);
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });

  app.request({
    method: "POST",
    url: urlxampp + "transaction/show_menu_cart.php", 
    data: {  
      menu_id : menu_id,
      store_id : localStorage.store_id
    },
    success: function(data) {
      console.log(data);
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        $$('#transaction_detail_count_transaction_menu_detail').val(x[0]['transaction_detail_count']);
      }
    },
    error: function(data) {
      console.log(data);
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });

  $$('#min_transaction_detail_count_transaction_menu_detail').on('click', function() {
    var count = parseInt($$('#transaction_detail_count_transaction_menu_detail').val());
    count -= 1;

    if(count <= 0) {
      count = 0;
    }
    $$('#transaction_detail_count_transaction_menu_detail').val(count);
  });

  $$('#add_transaction_detail_count_transaction_menu_detail').on('click', function() {
    var count = parseInt($$('#transaction_detail_count_transaction_menu_detail').val());
    count += 1;

    $$('#transaction_detail_count_transaction_menu_detail').val(count);
  });

  $$('#transaction_detail_count_transaction_menu_detail').on('keyup', function() {
    var count = parseInt($$('#transaction_detail_count_transaction_menu_detail').val());
    $$('#transaction_detail_count_transaction_menu_detail').val(count);
  });

  $$('#save_transaction_detail_transaction_menu_detail').on('click', function() {
    var transaction_detail_count = parseInt($$('#transaction_detail_count_transaction_menu_detail').val());

    if(transaction_detail_count <= 0) {
      app.dialog.alert('Minimal jumlah pesanan adalah 1!');
    } else {
      app.request({
        method: "POST",
        url: urlxampp + "transaction/insert.php", 
        data: {  
          menu_id : menu_id,
          transaction_detail_count : transaction_detail_count,
          user_id : localStorage.user_id,
          store_id : localStorage.store_id
        },
        success: function(data) {
          determinateLoading = false;
          app.dialog.close();
          page.router.navigate('/transaction/menu',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
        },
        error: function(data) {
          determinateLoading = false;
          app.dialog.close();
          var toastBottom = app.toast.create({
            text: error_connection,
            closeTimeout: 2000,
          });
          toastBottom.open();
          page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
        }
      });
    }
  });
}