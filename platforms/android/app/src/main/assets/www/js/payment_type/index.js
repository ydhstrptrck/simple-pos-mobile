function load_payment_type_index(e, page) {
  loadingData();
  
  app.request({
    method: "GET",
    url: urlxampp + "payment_type/select.php", 
    data: {  

    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        load_list_payment_type(x);
      } else {
        $$('#list_payment_type').html(`
          <div class="row">
            <div class="col">
              <div class="container-image">
                <img src="img/list-menu-empty.png" style="width: 100%; height: 65px;">
                <div class="center-of-image-list-menu" style="color: black; text-align: left;">
                  ` + obj['message'] + ` 
                </div>
              </div>
            </div>
          </div>
        `);
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });

  $$('#txt_search_payment_type').on('keyup', function() {
    app.request({
      method: "GET",
      url: urlxampp + "payment_type/select.php", 
      data:{ 
        keyword : $$('#txt_search_payment_type').val()
      },
      success: function(data) {
        var obj = JSON.parse(data);
        determinateLoading = false;
        app.dialog.close();
        if(obj['status'] == true) {
          var x = obj['data'];
          load_list_payment_type(x);
        } else {
          $$('#list_payment_type').html(`
            <div class="row">
              <div class="col">
                <div class="container-image">
                  <img src="img/list-menu-empty.png" style="width: 100%; height: 65px;">
                  <div class="center-of-image-list-menu" style="color: black; text-align: left;">
                    ` + obj['message'] + ` 
                  </div>
                </div>
              </div>
            </div>
          `);
        }
      },
      error: function(data) {
        determinateLoading = false;
        app.dialog.close();
        var toastBottom = app.toast.create({
          text: error_connection,
          closeTimeout: 2000,
        });
        toastBottom.open();
        page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
      }
    });
  });
}

function load_list_payment_type(x) {
  $$('#list_payment_type').html('');
  for(var i = 0; i < x.length; i++) {
    $$('#list_payment_type').append(`
      <div class="row">
        <div class="col">
          <div class="container-image">
            <img src="img/list-menu.png" style="width: 100%; height: 110px;">
            <div class="top-left-of-image-list-menu" style="color: black; text-align: left;">
              <span style="font-size: 16px;"><b>` + x[i]['payment_type_name'] + `</b></span>` +
            `</div>
            <div class="bottom-right-of-image-list-menu">
              <a class="link" href="/payment_type/edit/` + x[i]['payment_type_id'] + `">
                <img src="img/icon-edit.png" style="width: 80%;">
              </a> <br>
              <a class="link delete_payment_type" data-id="` + x[i]['payment_type_id'] + `">
                <img src="img/icon-delete.png" style="width: 80%;">
              </a>
            </div>
          </div>
        </div>
      </div>
    `);
  }

  $$('.delete_payment_type').on('click', function () {
    var id = $$(this).data('id');
    app.dialog.confirm("Apakah Anda yakin untuk menghapus jenis pembayaran ini?", 'Konfirmasi', function() {
      loadingData();
      app.request({
        method:"POST",
        url: urlxampp + "payment_type/delete.php",
        data: {
          payment_type_id : id
        },
        success:function(data){
          var obj = JSON.parse(data);
          determinateLoading = false;
          app.dialog.close();
          if(obj['status'] == true) {
            var x = obj['data'];
            app.dialog.alert(x, 'Notifikasi', function() {
              mainView.router.refreshPage();
            });
          } else {
            app.dialog.alert(obj['message']);
          }
        },
        error:function(data){
          determinateLoading = false;
          app.dialog.close();
          var toastBottom = app.toast.create({
            text: ERRNC,
            closeTimeout: 2000,
          });
          toastBottom.open();
          page.router.navigate('/user_home/',{ animate:false, reloadAll:true, force: true, ignoreCache: true });
        }
      });
    });
  });
}