function load_transaction_index (e, page) {
  loadingData();

  var calendarRange = app.calendar.create({
    inputEl: '#choose_date_transaction_index',
    rangePicker: true,
    openIn: 'customModal',
    header: true,
    footer: true,
  });

  app.request({
    method: "GET",
    url: urlxampp + "store/select.php", 
    data: {  
      
    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        for(var i = 0; i < x.length; i++) {
          $$('#choose_store_transaction_index').append(`
            <option value="` + x[i]['store_id'] + `">` + x[i]['store_name'] + `</option>
          `);
        }
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });

  var store_id  = "";
  if(localStorage.user_role == "Admin") {
    store_id = $$('#choose_store_transaction_index').val();
  } else {
    $$('.menu_admin').hide();
    store_id = localStorage.store_id;
  }

  app.request({
    method: "POST",
    url: urlxampp + "transaction/select_history.php", 
    data: {  
      date : $$('#choose_date_transaction_index').val(),
      store_id : store_id
    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        load_list_transaction(x);
      } else {
        $$('#list_transaction').html(`
          <div class="row">
            <div class="col">
              <div class="container-image">
                <img src="img/list-menu-empty.png" style="width: 100%; height: 65px;">
                <div class="center-of-image-list-menu" style="color: black; text-align: left;">
                  ` + obj['message'] + ` 
                </div>
              </div>
            </div>
          </div>
        `);
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });
  
  $$('#choose_date_transaction_index').on('change', function() {
    if(localStorage.user_role == "Admin") {
      store_id = $$('#choose_store_transaction_index').val();
    } else {
      store_id = localStorage.store_id;
    }

    app.request({
      method: "POST",
      url: urlxampp + "transaction/select_history.php", 
      data: {  
        date : $$('#choose_date_transaction_index').val(),
        store_id : store_id
      },
      success: function(data) {
        var obj = JSON.parse(data);
        determinateLoading = false;
        app.dialog.close();
        if(obj['status'] == true) {
          var x = obj['data'];
          load_list_transaction(x);
        } else {
          $$('#list_transaction').html(`
            <div class="row">
              <div class="col">
                <div class="container-image">
                  <img src="img/list-menu-empty.png" style="width: 100%; height: 65px;">
                  <div class="center-of-image-list-menu" style="color: black; text-align: left;">
                    ` + obj['message'] + ` 
                  </div>
                </div>
              </div>
            </div>
          `);
        }
      },
      error: function(data) {
        determinateLoading = false;
        app.dialog.close();
        var toastBottom = app.toast.create({
          text: error_connection,
          closeTimeout: 2000,
        });
        toastBottom.open();
        page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
      }
    });
  });

  $$('#choose_store_transaction_index').on('change', function() {
    if(localStorage.user_role == "Admin") {
      store_id = $$('#choose_store_transaction_index').val();
    } else {
      store_id = localStorage.store_id;
    }
  
    app.request({
      method: "POST",
      url: urlxampp + "transaction/select_history.php", 
      data: {  
        date : $$('#choose_date_transaction_index').val(),
        store_id : store_id
      },
      success: function(data) {
        var obj = JSON.parse(data);
        determinateLoading = false;
        app.dialog.close();
        if(obj['status'] == true) {
          var x = obj['data'];
          load_list_transaction(x);
        } else {
          $$('#list_transaction').html(`
            <div class="row">
              <div class="col">
                <div class="container-image">
                  <img src="img/list-menu-empty.png" style="width: 100%; height: 65px;">
                  <div class="center-of-image-list-menu" style="color: black; text-align: left;">
                    ` + obj['message'] + ` 
                  </div>
                </div>
              </div>
            </div>
          `);
        }
      },
      error: function(data) {
        determinateLoading = false;
        app.dialog.close();
        var toastBottom = app.toast.create({
          text: error_connection,
          closeTimeout: 2000,
        });
        toastBottom.open();
        page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
      }
    });
  });
}

function load_list_transaction(x) {
  $$('#list_transaction').html('');
  for(var i = 0; i < x.length; i++) {
    var action = "";
    if(localStorage.user_role == "Admin") {
      action = `<div class="bottom-right-of-image-list-menu">
          <a class="link delete_transaction" data-id="` + x[i]['transaction_id'] + `">
            <img src="img/icon-delete.png" style="width: 80%;">
          </a>
        </div>`;
    }
    
    $$('#list_transaction').append(`
      <div class="row">
        <div class="col">
          <a href="/transaction/invoice/` + x[i]['transaction_id'] + `">
            <div class="container-image">
              <img src="img/list-riwayat-penjualan.png" style="width: 100%; height: 150px;">
              <div class="top-left-of-image-list-menu" style="color: black; text-align: left; top: 20px;">
                <span style="font-size: 16px;"><b>[` + x[i]['transaction_invoice_number_formatted'] + `]</b></span>` +
                ` <br> Pendapatan : ` + formatRupiah(x[i]['transaction_grand_total']) + 
                ` <br> Kasir : ` + x[i]['user_name'] + 
                ` <br> ` + x[i]['store_name'] + 
                ` <br> ` + x[i]['transaction_date_formatted'] + ` ` + x[i]['transaction_time'] + `
              </div>
              ` + action + `
            </div>
          </a>
        </div>
      </div>
    `);
  }

  $$('.delete_transaction').on('click', function () {
    var id = $$(this).data('id');
    app.dialog.confirm("Apakah Anda yakin untuk menghapus transaksi ini?", 'Konfirmasi', function() {
      loadingData();
      app.request({
        method:"POST",
        url: urlxampp + "transaction/delete.php",
        data: {
          transaction_id : id
        },
        success:function(data){
          var obj = JSON.parse(data);
          determinateLoading = false;
          app.dialog.close();
          if(obj['status'] == true) {
            var x = obj['data'];
            app.dialog.alert(x, 'Notifikasi', function() {
              mainView.router.refreshPage();
            });
          } else {
            app.dialog.alert(obj['message']);
          }
        },
        error:function(data){
          determinateLoading = false;
          app.dialog.close();
          var toastBottom = app.toast.create({
            text: ERRNC,
            closeTimeout: 2000,
          });
          toastBottom.open();
          page.router.navigate('/user_home/',{ animate:false, reloadAll:true, force: true, ignoreCache: true });
        }
      });
    });
  });
}