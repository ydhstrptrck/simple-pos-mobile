var $$ = Dom7;
// username : pointofs
// password : W15kqWit24 
var urlxampp = "https://pointofsalestrk.my.id/simplepos/";

// var urlxampp = "http://192.168.18.60/simple-pos-mobile/php/";
var error_connection = "Connection timeout!";

var app = new Framework7({
	root: '#app',
	name: 'POS',
	id: 'com.simple.pos',
	theme: 'md',
	routes: [
		// LOGIN
		{
			path: '/user_login/',
			url: 'page/user_login.html',
			on: {
				pageInit: function(e, page) {
					$$('#btn-submit-login').on('click', function() {
						if($$('#username').val() == '' || $$('#password').val() == '') {
							app.dialog.alert("Mohon masukkan username dan/atau password!");
						} else {
							loadingData();
							var username = $$('#username').val();
							var password = $$('#password').val();

							app.request({
							    method: "POST",
							    url: urlxampp + "login.php", 
							    data: { 
							    	username : username, 
							    	password : password 
							    },
							    success: function(data) {
							      	var obj = JSON.parse(data);
							      	if(obj['status'] == true) {
												var obj = obj['data'];
												localStorage.user_id = obj['user_id'];
												localStorage.username = obj['username'];
												localStorage.user_name = obj['user_name'];
												localStorage.user_phone = obj['user_phone'];
												localStorage.email = obj['email'];
												localStorage.user_role = obj['user_role'];
												localStorage.store_id = obj['store_id'];
												page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
							      	} else {
												app.dialog.alert(obj['message']);
							      	}
						        	determinateLoading = false;
											app.dialog.close();
							    },
							    error: function(data) {
							      	determinateLoading = false;
							      	app.dialog.close();
							      	var toastBottom = app.toast.create({
								        text: error_connection + json.stringify(data),
								        closeTimeout: 2000,
							      	});
							      	toastBottom.open();
							      	page.router.navigate('/user_login/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
							    }
							});
						}
					});
				},
				pageAfterIn: function (event, page)
				{
					if(!localStorage.username) {
						page.router.navigate('/user_login/',{ animate:false, reloadAll:true });
					} else {
						page.router.navigate('/user_home/',{ animate:false, reloadAll:true });
					}
				}
			} 
		},
		// HOME
		{
			path: '/user_home/',
			url: 'page/user_home.html',
			on: {
				pageInit: function(e,page) {
					app.request({
			          method: "POST",
			          url: urlxampp + "user/show.php", 
			          data: {  
			            user_id : localStorage.user_id
			          },
			          success: function(data) {
			            var obj = JSON.parse(data);
			            if(obj['status'] == true) {
			            	if(obj['data'][0]['user_delete'] == 0) {
				            	localStorage.user_id = obj['data'][0]['user_id'];
											localStorage.username = obj['data'][0]['username'];
											localStorage.user_name = obj['data'][0]['user_name'];
											localStorage.user_phone = obj['data'][0]['user_phone'];
											localStorage.email = obj['data'][0]['email'];
											localStorage.user_role = obj['data'][0]['user_role'];
											localStorage.store_id = obj['data'][0]['store_id'];
			            	} else {
			            		localStorage.clear();
											page.router.navigate('/user_login/');
			            	}
			            }
			          },
			          error: function(data) {
			            determinateLoading = false;
			            app.dialog.close();
			            var toastBottom = app.toast.create({
						        text: error_connection + json.stringify(data),
			              closeTimeout: 2000,
			            });
			            toastBottom.open();
			            page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
			          }
			        });

					if(localStorage.user_role == "Admin") {
						$$('.menu_karyawan').hide();
					} else if(localStorage.user_role == "Karyawan") {
						$$('.menu_admin').hide();

						if(!localStorage.printer_name) {
							$$('#printer_name').html('<span style="color: red;">---</span>');
							$$('#printer_id').html('<span style="color: red;">---</span>');
							$$('#printer_service').html('<span style="color: red;">---</span>');
							$$('#printer_characteristic').html('<span style="color: red;">---</span>');
						} else {
							$$('#printer_name').html(localStorage.printer_name);
							$$('#printer_id').html(localStorage.printer_id);
							$$('#printer_service').html(localStorage.printer_service);
							$$('#printer_characteristic').html(localStorage.printer_characteristic);
						}
					}

					var printer_count = 1;
					$$('#scan_printer').on('click', function () {
					  	function successIsEnabled(message) {
							loadingData();

							$$('#list_printer').html('<h5 style="text-align: center;">Silahkan pilih salah satu bluetooth di bawah ini:</h5>');

							console.log("Bluetooth is Enabled! Message : " + JSON.stringify(message));

							function successScan(device) {
								console.log("Successfully Scanning! Device : " + JSON.stringify(device));

								var device_printer = JSON.parse(JSON.stringify(device));
								var printer_name = "";
								if(device_printer['name'] !== undefined) {
									printer_name = device_printer['name'];
								} else {
									printer_name = device_printer['id'];
								}

								$$('#list_printer').append(`
							        <div class="row">
								        <div class="col">
							        		<a id="printer` + printer_count + `" data-id="` + device_printer['id'] + 
								        	`" data-name="` + printer_name + `">
								              	<div class="container-image">
								                	<img src="img/btn-bluetooth.png" style="width: 100%; height: 65px; top: 45%;">
								                	<div class="center-of-image-list-menu" style="color: black; text-align: left;">
								                  		<b>` + printer_name + `</b>
								                	</div>
								              	</div>
								        	</a>
								        </div>
							        </div>
								`);

								$$('#printer' + printer_count).on('click', function() {
									var printer_namee = $$(this).data('name');
									var printer_idd = $$(this).data('id');
									app.dialog.confirm("Apakah Anda yakin memilih perangkat " + printer_namee + " ini sebagai printer?", function() {
										loadingData();

										localStorage.printer_name = printer_namee;
										localStorage.printer_id = printer_idd;
										$$('#list_printer').html('');

										function successConnect(message) {
											console.log("Successfully Connecting! Message : " + JSON.stringify(message));
											
											localStorage.printer_service = "-";
											localStorage.printer_characteristic = "-";
											var detail_printer = JSON.parse(JSON.stringify(message));
											for(var i = 0; i < detail_printer['characteristics'].length; i++) {
												for(var j = 0; j < detail_printer['characteristics'][i]['properties'].length; j++) {
													if(detail_printer['characteristics'][i]['properties'][j] == "Write") {
														localStorage.printer_service = detail_printer['characteristics'][i]['service'];
														localStorage.printer_characteristic = detail_printer['characteristics'][i]['characteristic'];
														break;
													}
												}
											}											

											$$('#printer_name').html(localStorage.printer_name);
											$$('#printer_id').html(localStorage.printer_id);
											$$('#printer_service').html(localStorage.printer_service);
											$$('#printer_characteristic').html(localStorage.printer_characteristic);

											determinateLoading = false;
											app.dialog.close();

											function successDisconnect(message) {
												console.log("Successfully Disconnecting! Message : " + JSON.stringify(message));
											};

											function failureDisconnect(error) {
												console.log("Error Disconnecting! Message : " + JSON.stringify(error));
											};

											ble.disconnect(localStorage.printer_id, successDisconnect, failureDisconnect);
											
											app.dialog.alert('Printer berhasil diatur!');
										};
										function failureConnect(error) {
											determinateLoading = false;
											app.dialog.close(); 
											console.log("Error Connecting! Message : " + JSON.stringify(error));
											app.dialog.alert("Printer not found or unreachable!");
										};

										ble.connect(localStorage.printer_id, successConnect, failureConnect);
									});
								});

								printer_count++;
							};

							function failureScan(error) {
								determinateLoading = false;
								app.dialog.close();
								console.log("Error Scanning! Message : " + JSON.stringify(error));
							};

							ble.startScan([], successScan, failureScan);

							setTimeout(ble.stopScan,
								5000,
								function() {
									console.log("Scan Complete!"); 
									if(printer_count == 1) {
										$$('#list_printer').html('<h5 style="text-align: center; margin-bottom: 0px;">Silahkan pilih salah satu bluetooth di bawah ini:</h5>');
										$$('#list_printer').append('<h5 style="text-align: center; margin-top: 0px; color: red;">Tidak ada perangkat bluetooth ditemukan.</h5>');
									}
									determinateLoading = false;
									app.dialog.close();
								},
								function() { 
									console.log("Stop Scan Failed!"); 
									determinateLoading = false;
									app.dialog.close();
								}
							);
						};

						function failureIsEnabled(error) {
							determinateLoading = false;
							app.dialog.close();
							console.log("Bluetooth is Disabled! Message : " + error); 
							app.dialog.alert("Silahkan nyalakan bluetooth anda terlebih dahulu!");
						};

						ble.isEnabled(successIsEnabled, failureIsEnabled);
					});	
				},
				pageAfterIn: function (event, page) {
					if(!localStorage.username) {
						page.router.navigate('/user_login/',{ animate:false, reloadAll:true });
					} else {
						page.router.navigate('/user_home/',{ animate:false, reloadAll:true });
					}

					$$('#logout').on('click', function () {
					  	app.dialog.confirm('Apakah Anda yakin ingin keluar dari aplikasi ini?', 'Keluar', function () {
						    localStorage.clear();
							page.router.navigate('/user_login/');
					  	});
					});
				}
			}
		},
		// USER INDEX
		{
			path: '/user/index',
			url: 'page/user/index.html',
			on: {
				pageInit: function(e, page) {
					load_user_index(e, page);
				}
			}
		},
		// USER CREATE
		{
			path: '/user/create',
			url: 'page/user/create.html',
			on: {
				pageInit: function(e, page) {
					load_user_create(e, page);
				}
			}
		},
		// USER EDIT
		{
			path: '/user/edit/:user_id',
			url: 'page/user/edit.html',
			on: {
				pageInit: function(e, page) {
					load_user_edit(e, page);
				}
			}
		},
		// STORE INDEX
		{
			path: '/store/index',
			url: 'page/store/index.html',
			on: {
				pageInit: function(e, page) {
					load_store_index(e, page);
				}
			}
		},
		// STORE CREATE
		{
			path: '/store/create',
			url: 'page/store/create.html',
			on: {
				pageInit: function(e, page) {
					load_store_create(e, page);
				}
			}
		},
		// STORE EDIT
		{
			path: '/store/edit/:store_id',
			url: 'page/store/edit.html',
			on: {
				pageInit: function(e, page) {
					load_store_edit(e, page);
				}
			}
		},
		// PAYMENT TYPE INDEX
		{
			path: '/payment_type/index',
			url: 'page/payment_type/index.html',
			on: {
				pageInit: function(e, page) {
					load_payment_type_index(e, page);
				}
			}
		},
		// PAYMENT TYPE CREATE
		{
			path: '/payment_type/create',
			url: 'page/payment_type/create.html',
			on: {
				pageInit: function(e, page) {
					load_payment_type_create(e, page);
				}
			}
		},
		// PAYMENT TYPE EDIT
		{
			path: '/payment_type/edit/:payment_type_id',
			url: 'page/payment_type/edit.html',
			on: {
				pageInit: function(e, page) {
					load_payment_type_edit(e, page);
				}
			}
		},
		// MENU INDEX
		{
			path: '/menu/index',
			url: 'page/menu/index.html',
			on: {
				pageInit: function(e, page) {
					load_menu_index(e, page);
				}
			}
		},
		// MENU CREATE
		{
			path: '/menu/create',
			url: 'page/menu/create.html',
			on: {
				pageInit: function(e, page) {
					load_menu_create(e, page);
				}
			}
		},
		// MENU EDIT
		{
			path: '/menu/edit/:menu_id',
			url: 'page/menu/edit.html',
			on: {
				pageInit: function(e, page) {
					load_menu_edit(e, page);
				}
			}
		},
		// TRANSACTION MENU
		{
			path: '/transaction/menu',
			url: 'page/transaction/menu.html',
			on: {
				pageInit: function(e, page) {
					load_transaction_menu(e, page);
				}
			}
		},
		// TRANSACTION MENU DETAIL
		{
			path: '/transaction/menu_detail/:menu_id',
			url: 'page/transaction/menu_detail.html',
			on: {
				pageInit: function(e, page) {
					load_transaction_menu_detail(e, page);
				}
			}
		},
		// TRANSACTION CART
		{
			path: '/transaction/cart',
			url: 'page/transaction/cart.html',
			on: {
				pageInit: function(e, page) {
					load_transaction_cart(e, page);
				}
			}
		},
		// TRANSACTION INVOICE
		{
			path: '/transaction/invoice/:transaction_id',
			url: 'page/transaction/invoice.html',
			on: {
				pageInit: function(e, page) {
					load_transaction_invoice(e, page);
				}
			}
		},
		// TRANSACTION INDEX
		{
			path: '/transaction/index',
			url: 'page/transaction/index.html',
			on: {
				pageInit: function(e, page) {
					load_transaction_index(e, page);
				}
			}
		},
		// TRANSACTION REPORT
		{
			path: '/transaction/report',
			url: 'page/transaction/report.html',
			on: {
				pageInit: function(e, page) {
					load_transaction_report(e, page);
				}
			}
		},
	]
});

var mainView = app.views.create('.view-main', {
  	url: '/user_home/'	
});

function onBackKeyDown() {
	if(app.views.main.history.length == 1 || app.views.main.router.url == '/user_home/') {
		navigator.app.exitApp();
	} else {
		if(app.views.main.router.url == '/user_login/') {  
			navigator.app.exitApp();
		} else {
			app.dialog.close();
			// app.views.main.router.back();
			app.views.main.router.back({
				url: /user_home/,
				force: true,
				ignoreCache: true
			});
			return false;
		}
	}
}
document.addEventListener("backbutton", onBackKeyDown, false);

function loadingData() {
	showDeterminate(true);
	determinateLoading = false;
	function showDeterminate(inline) {
		determinateLoading = true;
		var progressBarEl;
		if (inline) {
			progressBarEl = app.dialog.progress("Loading");
		} else {
			progressBarEl = app.progressbar.show(0, app.theme === 'md' ? 'yellow' : 'blue');
		}
		function simulateLoading() {
			setTimeout(function () {
				simulateLoading();
			}, Math.random() * 300 + 300);
		}
		simulateLoading();
	}
}

function str2ab(str) {
  var buf = new ArrayBuffer(str.length * 2);
  var bufView = new Uint16Array(buf);
  for (var i = 0, strLen = str.length; i < strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}

function formatRupiah(angka){
	var number_string = angka.toString();
	sisa  = number_string.length % 3;
	rupiah  = number_string.substr(0, sisa);
	ribuan  = number_string.substr(sisa).match(/\d{3}/g);

	if (ribuan) {
	 	separator = sisa ? '.' : '';
	 	rupiah += separator + ribuan.join('.');
	}
	return 'IDR '+rupiah;
}

function formatStock(stock){
	var number_string = stock.toString();
	sisa  = number_string.length % 3;
	stok  = number_string.substr(0, sisa);
	ribuan  = number_string.substr(sisa).match(/\d{3}/g);

	if (ribuan) {
	 	separator = sisa ? '.' : '';
	 	stok += separator + ribuan.join('.');
	}
	return stok + " buah";
}

function formatKeuntungan(harga_beli, harga_jual){
	var untung = parseInt(harga_jual) - parseInt(harga_beli);
	var keuntungan = parseInt(untung) / parseInt(harga_beli) * 100;
	keuntungan = keuntungan.toFixed(2);
	return keuntungan + "%";
}