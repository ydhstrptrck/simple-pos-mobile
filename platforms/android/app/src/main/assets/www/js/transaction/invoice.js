function load_transaction_invoice (e, page) {
  var transaction_id = page.router.currentRoute.params.transaction_id;
  loadingData();

  app.request({
    method: "GET",
    url: urlxampp + "payment_type/select.php", 
    data: {  

    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        for(var i = 0; i < x.length; i++) {
          $$('#payment_type_id_transaction_invoice').append(`
            <option value="` + x[i]['payment_type_id'] + `">` + x[i]['payment_type_name'] + `</option>
          `);
        }
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });

  var total_amount = 0;
  var total_discount = 0;
  var grand_total = 0;
  var total_paid = 0;
  var total_change = 0;

  var user_name = "";
  var transaction_customer_name = "";
  var transaction_order_number = "";
  var payment_type_name = "";
  var transaction_message = "";
  var transaction_date_time = "";

  var store_name = "";
  var store_address = "";

  var menu_name = [];
  var transaction_detail_price = [];
  var transaction_detail_count = [];
  var transaction_detail_discount = [];
  var transaction_subtotal = [];

  app.request({
    method: "POST",
    url: urlxampp + "transaction/select.php", 
    data: {  
      transaction_id : transaction_id,
      store_id : 0
    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];

        $$('#transaction_invoice_number_transaction_invoice').html(
          x[0]['transaction_invoice_number_formatted']
        );

        var tbody = "";
        total_amount = formatRupiah(x[0]['transaction_total_amount'].toString());
        total_discount = formatRupiah(x[0]['transaction_total_discount'].toString());
        grand_total = formatRupiah((parseInt(x[0]['transaction_total_amount']) - parseInt(x[0]['transaction_total_discount'])).toString());
        total_paid = formatRupiah(x[0]['transaction_total_paid'].toString());
        total_change = formatRupiah(x[0]['transaction_total_change'].toString());

        store_name = x[0]['store_name'];
        store_address = x[0]['store_address'];

        user_name = x[0]['user_name'];
        transaction_customer_name = x[0]['transaction_customer_name'];
        transaction_order_number = x[0]['transaction_order_number_formatted'];
        payment_type_name = x[0]['payment_type_name'];
        transaction_message = x[0]['transaction_message'];
        transaction_date_time = x[0]['transaction_date_formatted'] + ' ' + x[0]['transaction_time'];

        for(var i = 0; i < x.length; i++) {
          var subtotal = parseInt(x[i]['transaction_detail_price']) * parseInt(x[i]['transaction_detail_count']);
          var subtotal_discount = subtotal * parseInt(x[i]['transaction_detail_discount']) / 100;
          var subtotal_after_discount = subtotal - subtotal_discount;

          menu_name.push(x[i]['menu_name']);
          transaction_detail_price.push(formatRupiah(x[i]['transaction_detail_price']));
          transaction_detail_count.push(x[i]['transaction_detail_count']);
          transaction_detail_discount.push('(' + x[i]['transaction_detail_discount'] + '%)');
          transaction_subtotal.push(formatRupiah(subtotal_after_discount.toString()));

          tbody += `
            <tr>
              <td>
                ` + x[i]['menu_name'] + `
                <br>` + x[i]['transaction_detail_count'] + ` x ` + formatRupiah(x[i]['transaction_detail_price']) + `
                <br>Disc ` + x[i]['transaction_detail_discount'] + `%` +
              `</td>
              <td>` + formatRupiah(subtotal_after_discount) + `</td>
            </tr>
            `;
        }

        $$('#transaction_total_amount_transaction_invoice').html(
          total_amount
        );
        $$('#transaction_total_discount_transaction_invoice').html(
          total_discount
        );
        $$('#transaction_grand_total_transaction_invoice').html(
          grand_total
        );
        $$('#transaction_total_paid_transaction_invoice').html(
          total_paid
        );
        $$('#transaction_total_change_transaction_invoice').html(
          total_change
        );

        $$('#user_name_transaction_invoice').html(
          user_name
        );
        $$('#transaction_customer_name_transaction_invoice').html(
          transaction_customer_name
        );
        $$('#payment_type_name_transaction_invoice').html(
          payment_type_name
        );
        $$('#transaction_message_transaction_invoice').html(
          transaction_message
        );
        $$('#transaction_date_transaction_invoice').html(
          transaction_date_time
        );

        $$('#tbody_transaction_invoice').html(tbody);

        $$('#btn_print_transaction_invoice').on('click', function() {
          if(!localStorage.printer_id) {
            app.dialog.alert('Silahkan scan dan pilih perangkat printer di halaman beranda terlebih dahulu!');
          } else {
            var device_id = localStorage.printer_id;
    
            var enter = "\n";
            var enterAB = str2ab(enter);
            function successIsEnabled(message) {
              loadingData();
              console.log("Bluetooth is Enabled! Message : " + JSON.stringify(message));

              function successScan(device) {
                console.log("Successfully Scanning! Device : " + JSON.stringify(device));
              };

              function failureScan(error) {
                determinateLoading = false;
                app.dialog.close();
                console.log("Error Scanning! Message : " + JSON.stringify(error));
              };

              ble.startScan([], successScan, failureScan);
              
              setTimeout(ble.stopScan,
                5000,
                function() {
                  console.log("Scan Complete!"); 

                  function successConnect(message) {
                    determinateLoading = false;
                    app.dialog.close();
                    
                    console.log("Successfully Connecting! Message : " + JSON.stringify(message));
                    
                    function successWrite(message) {
                      console.log("Successfully Write! Message : " + JSON.stringify(message));
                    };

                    function successLastWrite(message) {
                      console.log("Successfully Write! Message : " + JSON.stringify(message));
                      function successDisconnect(message) {
                        console.log("Successfully Disconnecting! Message : " + JSON.stringify(message));
                      };

                      function failureDisconnect(error) {
                        console.log("Error Disconnecting! Message : " + JSON.stringify(error));
                      };

                      ble.disconnect(device_id, successDisconnect, failureDisconnect);
                    };

                    function failureWrite(error) {
                      console.log("Error failureWrite! Message : " + JSON.stringify(error));
                      function successDisconnect(message) {
                        console.log("Successfully Disconnecting! Message : " + JSON.stringify(message));
                      };

                      function failureDisconnect(error) {
                        console.log("Error Disconnecting! Message : " + JSON.stringify(error));
                      };

                      ble.disconnect(device_id, successDisconnect, failureDisconnect);
                    };
                    
                    var max = 30;
                    var space = 0;

                    if(parseInt(store_name.length) <= parseInt(max)) {
                      space = (parseInt(max) - parseInt(store_name.length)) / 2;

                      var space_store_name = "";
                      for(var i = 0; i < space; i++) {
                        space_store_name += " ";
                      }
                      store_name = space_store_name + store_name;
                    }

                    if(parseInt(store_address.length) <= parseInt(max)) {
                      space = (parseInt(max) - parseInt(store_address.length)) / 2;

                      var space_store_address = "";
                      for(var i = 0; i < space; i++) {
                        space_store_address += " ";
                      }
                      store_address = space_store_address + store_address;
                    }

                    if(parseInt(transaction_date_time.length) <= parseInt(max)) {
                      space = (parseInt(max) - parseInt(transaction_date_time.length)) / 2;

                      var space_transaction_date_time = "";
                      for(var i = 0; i < space; i++) {
                        space_transaction_date_time += " ";
                      }
                      transaction_date_time = space_transaction_date_time + transaction_date_time;
                    }

                    var store_nameAB = str2ab(store_name + " \n");
                    var store_addressAB = str2ab(store_address + " \n");
                    var transaction_date_timeAB = str2ab(transaction_date_time + " \n");
                    var user_nameAB = str2ab("Kasir     : " + user_name + " \n");
                    var transaction_customer_nameAB = str2ab("Pelanggan : " + transaction_customer_name + " \n");
                    var transaction_order_numberAB = str2ab("No Order  : " + transaction_order_number + " \n");

                    ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, store_nameAB, successWrite, failureWrite);
                    ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, store_addressAB, successWrite, failureWrite);
                    ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, transaction_date_timeAB, successWrite, failureWrite);
                    ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, enterAB, successWrite, failureWrite);
                    ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, user_nameAB, successWrite, failureWrite);
                    ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, transaction_customer_nameAB, successWrite, failureWrite);
                    ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, transaction_order_numberAB, successWrite, failureWrite);
                    ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, enterAB, successWrite, failureWrite);
                  
                    for(var i = 0; i < menu_name.length ; i++){
                      var menu_nameAB = str2ab(menu_name[i] + " \n");
                      ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, menu_nameAB, successWrite, failureWrite);
                      var dataBarangAB = "";
                      if(transaction_detail_discount[i] == "(0%)") {
                        dataBarangAB = str2ab("    " + transaction_detail_price[i] + " x " + transaction_detail_count[i] + " \n");
                      } else {
                        dataBarangAB = str2ab("    " + transaction_detail_price[i] + " - " + transaction_detail_discount[i]  + " x " + transaction_detail_count[i] + " \n");
                      }
                      ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, dataBarangAB, successWrite, failureWrite);
                      var transaction_subtotalAB = str2ab("    " + transaction_subtotal[i] + " \n");
                      ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, transaction_subtotalAB, successWrite, failureWrite);
                    }
                    ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, enterAB, successWrite, failureWrite);

                    if(total_discount != "IDR 0") {
                      var total_amountAB = str2ab("Total      : " + total_amount + "\n");
                      ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, total_amountAB, successWrite, failureWrite);
                      var total_discountAB = str2ab("Diskon     : " + total_discount + "\n");
                      ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, total_discountAB, successWrite, failureWrite);
                      var grand_totalAB = str2ab("Grand Total: " + grand_total + "\n");
                      ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, grand_totalAB, successWrite, failureWrite);
                    } else {
                      var grand_totalAB = str2ab("Grand Total: " + grand_total + "\n");
                      ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, grand_totalAB, successWrite, failureWrite);
                    }
                    ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, enterAB, successWrite, failureWrite);

                    var total_paidAB = str2ab("Pembayaran : " + total_paid + "\n");
                    ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, total_paidAB, successWrite, failureWrite);
                    var total_changeAB = str2ab("Kembalian  : " + total_change + "\n");
                    ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, total_changeAB, successWrite, failureWrite);

                    ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, enterAB, successWrite, failureWrite);
                    ble.write(device_id, localStorage.printer_service, localStorage.printer_characteristic, enterAB, successLastWrite, failureWrite);
                  };

                  function failureConnect(error) {
                    determinateLoading = false;
                    app.dialog.close(); 
                    console.log("Error Connecting! Message : " + JSON.stringify(error));
                    app.dialog.alert("Printer not found or unreachable!");
                  };

                  ble.connect(device_id, successConnect, failureConnect);
                },
                function() { 
                  determinateLoading = false;
                  app.dialog.close();
                  console.log("Stop Scan Failed!"); 
                }
              );
            };

            function failureIsEnabled(error) {
              determinateLoading = false;
              app.dialog.close();
              console.log("Bluetooth is Disabled! Message : " + error); 
              app.dialog.alert("Silahkan nyalakan bluetooth anda terlebih dahulu!");
            };

            ble.isEnabled(successIsEnabled, failureIsEnabled);
          }
        });
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });
}