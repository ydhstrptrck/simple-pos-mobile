/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.simple.cashier;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.simple.cashier";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 10000;
  public static final String VERSION_NAME = "1.0.0";
}
