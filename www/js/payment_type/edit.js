function load_payment_type_edit(e, page) {
  var payment_type_id = page.router.currentRoute.params.payment_type_id;
  loadingData();

  app.request({
    method: "POST",
    url: urlxampp + "payment_type/show.php", 
    data: {  
      payment_type_id : payment_type_id
    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        $$('#payment_type_name_edit_payment_type').val(obj['data'][0]['payment_type_name']);
      } else {
        app.dialog.alert(obj['message'], 'Notifikasi', function(){
          page.router.navigate('/payment_type/index');
        });
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });
  
  $$('#btn_edit_payment_type').on('click', function() {
    var payment_type_name = $$('#payment_type_name_edit_payment_type').val();

    if(payment_type_name == "") {
      app.dialog.alert('Nama jenis pembayaran wajib diisi!');
    } else {
      app.request({
        method: "POST",
        url: urlxampp + "payment_type/update.php", 
        data: {  
          payment_type_id : payment_type_id,
          payment_type_name : payment_type_name
        },
        success: function(data) {
          var obj = JSON.parse(data);
          determinateLoading = false;
          app.dialog.close();
          if(obj['status'] == true) {
            app.dialog.alert(obj['data'], 'Notifikasi', function(){
              page.router.navigate('/payment_type/index');
            });
          } else {
            app.dialog.alert(obj['message']);
          }
        },
        error: function(data) {
          determinateLoading = false;
          app.dialog.close();
          var toastBottom = app.toast.create({
            text: error_connection,
            closeTimeout: 2000,
          });
          toastBottom.open();
          page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
        }
      });
    }
  });
}