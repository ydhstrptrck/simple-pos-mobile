function load_store_index(e, page) {
  loadingData();
  
  app.request({
    method: "GET",
    url: urlxampp + "store/select.php", 
    data: {  

    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        load_list_store(x);
      } else {
        $$('#list_store').html(`
          <div class="row">
            <div class="col">
              <div class="container-image">
                <img src="img/list-menu-empty.png" style="width: 100%; height: 65px;">
                <div class="center-of-image-list-menu" style="color: black; text-align: left;">
                  ` + obj['message'] + ` 
                </div>
              </div>
            </div>
          </div>
        `);
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });

  $$('#txt_search_store').on('keyup', function() {
    app.request({
      method: "GET",
      url: urlxampp + "store/select.php", 
      data:{ 
        keyword : $$('#txt_search_store').val()
      },
      success: function(data) {
        var obj = JSON.parse(data);
        determinateLoading = false;
        app.dialog.close();
        if(obj['status'] == true) {
          var x = obj['data'];
          load_list_store(x);
        } else {
          $$('#list_store').html(`
            <div class="row">
              <div class="col">
                <div class="container-image">
                  <img src="img/list-menu-empty.png" style="width: 100%; height: 65px;">
                  <div class="center-of-image-list-menu" style="color: black; text-align: left;">
                    ` + obj['message'] + ` 
                  </div>
                </div>
              </div>
            </div>
          `);
        }
      },
      error: function(data) {
        determinateLoading = false;
        app.dialog.close();
        var toastBottom = app.toast.create({
          text: error_connection,
          closeTimeout: 2000,
        });
        toastBottom.open();
        page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
      }
    });
  });
}

function load_list_store(x) {
  $$('#list_store').html('');
  for(var i = 0; i < x.length; i++) {
    $$('#list_store').append(`
      <div class="row">
        <div class="col">
          <div class="container-image">
            <img src="img/list-menu.png" style="width: 100%; height: 100px;">
            <div class="top-left-of-image-list-menu" style="color: black; text-align: left;">
              <span style="font-size: 16px;"><b>` + x[i]['store_name'] + `</b></span>` +
              ` <br> ` + x[i]['store_address'] + 
            `</div>
            <div class="bottom-right-of-image-list-menu">
              <a class="link" href="/store/edit/` + x[i]['store_id'] + `">
                <img src="img/icon-edit.png" style="width: 80%;">
              </a> <br>
              <a class="link delete_store" data-id="` + x[i]['store_id'] + `">
                <img src="img/icon-delete.png" style="width: 80%;">
              </a>
            </div>
          </div>
        </div>
      </div>
    `);
  }

  $$('.delete_store').on('click', function () {
    var id = $$(this).data('id');
    app.dialog.confirm("Apakah Anda yakin untuk menghapus usaha ini?", 'Konfirmasi', function() {
      loadingData();
      app.request({
        method:"POST",
        url: urlxampp + "store/delete.php",
        data: {
          store_id : id
        },
        success:function(data){
          var obj = JSON.parse(data);
          determinateLoading = false;
          app.dialog.close();
          if(obj['status'] == true) {
            var x = obj['data'];
            app.dialog.alert(x, 'Notifikasi', function() {
              mainView.router.refreshPage();
            });
          } else {
            app.dialog.alert(obj['message']);
          }
        },
        error:function(data){
          determinateLoading = false;
          app.dialog.close();
          var toastBottom = app.toast.create({
            text: ERRNC,
            closeTimeout: 2000,
          });
          toastBottom.open();
          page.router.navigate('/user_home/',{ animate:false, reloadAll:true, force: true, ignoreCache: true });
        }
      });
    });
  });
}