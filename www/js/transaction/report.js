function load_transaction_report (e, page) {
  loadingData();

  var calendarRange = app.calendar.create({
    inputEl: '#choose_date_transaction_report',
    rangePicker: true,
    openIn: 'customModal',
    header: true,
    footer: true,
  });

  app.request({
    method: "GET",
    url: urlxampp + "store/select.php", 
    data: {  
      
    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        for(var i = 0; i < x.length; i++) {
          $$('#choose_store_transaction_report').append(`
            <option value="` + x[i]['store_id'] + `">` + x[i]['store_name'] + `</option>
          `);
        }

        app.request({
        method: "POST",
        url: urlxampp + "transaction/select_report.php", 
        data: {  
          date : $$('#choose_date_transaction_report').val(),
          store_id : $$('#choose_store_transaction_report').val()
        },
        success: function(data) {
          var obj = JSON.parse(data);
          determinateLoading = false;
          app.dialog.close();
          if(obj['status'] == true) {
            var x = obj['data'];
            var x_report = obj['data_report'];
            load_report(x, x_report);
          } else {
            load_no_report();
          }
        },
        error: function(data) {
          determinateLoading = false;
          app.dialog.close();
          var toastBottom = app.toast.create({
            text: error_connection,
            closeTimeout: 2000,
          });
          toastBottom.open();
          page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
        }
      });
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });
  
  $$('#choose_date_transaction_report').on('change', function() {
    if(localStorage.user_role == "Admin") {
      store_id = $$('#choose_store_transaction_report').val();
    } else {
      store_id = localStorage.store_id;
    }

    app.request({
      method: "POST",
      url: urlxampp + "transaction/select_report.php", 
      data: {  
        date : $$('#choose_date_transaction_report').val(),
        store_id : store_id
      },
      success: function(data) {
        var obj = JSON.parse(data);
        determinateLoading = false;
        app.dialog.close();
        if(obj['status'] == true) {
          var x = obj['data'];
          var x_report = obj['data_report'];
          load_report(x, x_report);
        } else {
          load_no_report();
        }
      },
      error: function(data) {
        determinateLoading = false;
        app.dialog.close();
        var toastBottom = app.toast.create({
          text: error_connection,
          closeTimeout: 2000,
        });
        toastBottom.open();
        page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
      }
    });
  });

  $$('#choose_store_transaction_report').on('change', function() {
    if(localStorage.user_role == "Admin") {
      store_id = $$('#choose_store_transaction_report').val();
    } else {
      store_id = localStorage.store_id;
    }
  
    app.request({
      method: "POST",
      url: urlxampp + "transaction/select_report.php", 
      data: {  
        date : $$('#choose_date_transaction_report').val(),
        store_id : store_id
      },
      success: function(data) {
        var obj = JSON.parse(data);
        determinateLoading = false;
        app.dialog.close();
        if(obj['status'] == true) {
          var x = obj['data'];
          var x_report = obj['data_report'];
          load_report(x, x_report);
        } else {
          load_no_report();
        }
      },
      error: function(data) {
        determinateLoading = false;
        app.dialog.close();
        var toastBottom = app.toast.create({
          text: error_connection,
          closeTimeout: 2000,
        });
        toastBottom.open();
        page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
      }
    });
  });
}

function load_report(x, x_report) {
  $$('#total_income_transaction_report').html(
    formatRupiah(x_report['total_income'])
  );
  $$('#total_discount_transaction_report').html(
    formatRupiah(x_report['total_discount'])
  );
  $$('#total_income_after_discount_transaction_report').html(
    formatRupiah(x_report['total_income_after_discount'])
  );

  var tbody = "";
  for(var i = 0; i < x.length; i++) {
    tbody += `
      <tr>
        <td>` + x[i]['menu_name'] + `</td>
        <td>` + x[i]['transaction_detail_count_without_discount'] + `</td>
        <td>` + x[i]['transaction_detail_count_with_discount'] + `</td>
        <td>` + x[i]['transaction_detail_count_total'] + `</td>
        <td>` + formatRupiah(x[i]['transaction_detail_price_total']) + `</td>
      </tr>
      `;
  }
  $$('#tbody_transaction_report').html(tbody);
}

function load_no_report() {
  $$('#total_income_transaction_report').html(
    formatRupiah("0")
  );
  $$('#total_discount_transaction_report').html(
    formatRupiah("0")
  );
  $$('#total_income_after_discount_transaction_report').html(
    formatRupiah("0")
  );

  var tbody = `
    <tr>
      <td colspan='5'>Tidak ada penjualan</td>
    </tr>
    `;
  $$('#tbody_transaction_report').html(tbody);
}