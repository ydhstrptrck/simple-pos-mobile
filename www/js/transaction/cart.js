function load_transaction_cart (e, page) {
  loadingData();

  app.request({
    method: "GET",
    url: urlxampp + "payment_type/select.php", 
    data: {  

    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        for(var i = 0; i < x.length; i++) {
          $$('#payment_type_id_transaction_cart').append(`
            <option value="` + x[i]['payment_type_id'] + `">` + x[i]['payment_type_name'] + `</option>
          `);
        }
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });

  var total_amount = 0;
  var total_discount = 0;
  var grand_total = 0;

  app.request({
    method: "POST",
    url: urlxampp + "transaction/select.php", 
    data: {  
      transaction_id : 0,
      store_id : localStorage.store_id
    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        var tbody = "";
        total_amount = x[0]['transaction_total_amount'];
        total_discount = x[0]['transaction_total_discount'];
        grand_total = parseInt(x[0]['transaction_total_amount']) - parseInt(x[0]['transaction_total_discount']);

        for(var i = 0; i < x.length; i++) {
          var subtotal = parseInt(x[i]['transaction_detail_price']) * parseInt(x[i]['transaction_detail_count']);
          var subtotal_discount = subtotal * parseInt(x[i]['transaction_detail_discount']) / 100;
          var subtotal_after_discount = subtotal - subtotal_discount;

          tbody += `
            <tr>
              <td>
                ` + x[i]['menu_name'] + `
                <br>` + x[i]['transaction_detail_count'] + ` x ` + formatRupiah(x[i]['transaction_detail_price']) + `
                <br>Disc ` + x[i]['transaction_detail_discount'] + `%` +
              `</td>
              <td>` + formatRupiah(subtotal_after_discount) + `</td>
            </tr>
            `;
        }

        $$('#transaction_total_amount_transaction_cart').html(
          formatRupiah(total_amount)
        );
        $$('#transaction_total_discount_transaction_cart').html(
          formatRupiah(total_discount)
        );
        $$('#transaction_grand_total_transaction_cart').html(
          formatRupiah(grand_total)
        );

        $$('#tbody_transaction_cart').html(tbody);
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });

  $$('#btn_checkout_transaction_cart').on('click', function() {
    var payment_type_id = $$('#payment_type_id_transaction_cart').val();
    var transaction_customer_name = $$('#transaction_customer_name_transaction_cart').val();
    var transaction_total_paid = $$('#transaction_total_paid_transaction_cart').val();
    var transaction_message = $$('#transaction_message_transaction_cart').val();
    var transaction_total_change = parseInt(transaction_total_paid) - parseInt(grand_total);

    if(payment_type_id == "") {
      app.dialog.alert('Silahkan pilih jenis pembayaran terlebih dahulu!');
    } else if(transaction_customer_name == "") {
      app.dialog.alert('Nama pelanggan wajib diisi!');
    } else if(transaction_total_paid == "") {
      app.dialog.alert('Nominal pembayaran wajib diisi!');
    } else if(parseInt(transaction_total_paid) < parseInt(grand_total)) {
      app.dialog.alert('Nominal pembayaran yang Anda masukkan tidak cukup untuk melanjutkan transaksi ini!');
    } else {
      app.request({
        method: "POST",
        url: urlxampp + "transaction/update.php", 
        data: {  
          transaction_id : 0,
          payment_type_id : payment_type_id,
          transaction_customer_name : transaction_customer_name,
          transaction_message : transaction_message,
          transaction_total_paid : transaction_total_paid,
          transaction_total_change : transaction_total_change,
          user_id : localStorage.user_id,
          store_id : localStorage.store_id
        },
        success: function(data) {
          determinateLoading = false;
          app.dialog.close();
          var obj = JSON.parse(data);
          page.router.navigate('/transaction/invoice/' + obj['transaction_id'],{ animate:false, reloadAll:true , force: true, ignoreCache: true});
        },
        error: function(data) {
          determinateLoading = false;
          app.dialog.close();
          var toastBottom = app.toast.create({
            text: error_connection,
            closeTimeout: 2000,
          });
          toastBottom.open();
          page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
        }
      });
    }
  });
}