function load_transaction_menu (e, page) {
  loadingData();

  app.request({
    method: "POST",
    url: urlxampp + "transaction/select.php", 
    data: {  
      transaction_id : 0,
      store_id : localStorage.store_id
    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        if(x.length == 0) {
          $$('#btn_cart_transaction_menu').hide();
        } else {
          $$('#btn_cart_transaction_menu').show();
          $$('#count_transaction_detail_transaction_menu').html(x.length);
        }
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });
  
  app.request({
    method: "GET",
    url: urlxampp + "menu/select.php", 
    data: {  
      store_id : localStorage.store_id
    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        load_list_menu_transaction_menu(x);
      } else {
        $$('#list_menu_transaction_menu').html(`
          <div class="row">
            <div class="col">
              <div class="container-image">
                <img src="img/list-menu-empty.png" style="width: 100%; height: 65px;">
                <div class="center-of-image-list-menu" style="color: black; text-align: left;">
                  ` + obj['message'] + ` 
                </div>
              </div>
            </div>
          </div>
        `);
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });

  $$('#txt_search_menu_transaction_menu').on('keyup', function() {
    app.request({
      method: "GET",
      url: urlxampp + "menu/select.php", 
      data:{ 
        keyword : $$('#txt_search_menu_transaction_menu').val(),
        store_id : localStorage.store_id
      },
      success: function(data) {
        var obj = JSON.parse(data);
        determinateLoading = false;
        app.dialog.close();
        if(obj['status'] == true) {
          var x = obj['data'];
          load_list_menu_transaction_menu(x);
        } else {
          $$('#list_menu_transaction_menu').html(`
            <div class="row">
              <div class="col">
                <div class="container-image">
                  <img src="img/list-menu-empty.png" style="width: 100%; height: 65px;">
                  <div class="center-of-image-list-menu" style="color: black; text-align: left;">
                    ` + obj['message'] + ` 
                  </div>
                </div>
              </div>
            </div>
          `);
        }
      },
      error: function(data) {
        determinateLoading = false;
        app.dialog.close();
        var toastBottom = app.toast.create({
          text: error_connection,
          closeTimeout: 2000,
        });
        toastBottom.open();
        page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
      }
    });
  });
}

function load_list_menu_transaction_menu(x) {
  $$('#list_menu_transaction_menu').html('');
  for(var i = 0; i < x.length; i++) {
    var menu_sell_price = parseInt(x[i]['menu_sell_price']);
    var menu_discount = parseInt(x[i]['menu_discount']);
    var menu_sell_price_after_discount = "";
    if(menu_discount > 0) {
      menu_sell_price_after_discount = formatRupiah((menu_sell_price - (menu_sell_price * menu_discount / 100)).toString());
      menu_sell_price = "<strike>" + formatRupiah(menu_sell_price.toString()) + "</strike>";
    } else {
      menu_sell_price = formatRupiah(menu_sell_price.toString());
    }
    $$('#list_menu_transaction_menu').append(`
      <div class="row">
        <div class="col">
          <a href="/transaction/menu_detail/` + x[i]['menu_id'] + `">
            <div class="container-image">
              <img src="img/list-riwayat-penjualan.png" style="width: 100%; height: 90px;">
              <div class="top-left-of-image-list-menu" style="color: black; text-align: left; top: 20px;">
                <span style="font-size: 16px;"><b>` + x[i]['menu_name'] + `</b></span>` +
                ` <br> 1 x ` + menu_sell_price + ` ` + menu_sell_price_after_discount +
              `</div>
            </div>
          </a>
        </div>
      </div>
    `);
  }
}