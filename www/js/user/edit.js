function load_user_edit(e, page) {
  var user_id = page.router.currentRoute.params.user_id;
  loadingData();

  app.request({
    method: "GET",
    url: urlxampp + "store/select.php", 
    data: {  
      
    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        for(var i = 0; i < x.length; i++) {
          $$('#store_id_edit_user').append(`
            <option value="` + x[i]['store_id'] + `">` + x[i]['store_name'] + `</option>
          `);
        }

        app.request({
          method: "POST",
          url: urlxampp + "user/show.php", 
          data: {  
            user_id : user_id
          },
          success: function(data) {
            var obj = JSON.parse(data);
            determinateLoading = false;
            app.dialog.close();
            if(obj['status'] == true) {
              $$('#username_edit_user').val(obj['data'][0]['username']);
              $$('#user_name_edit_user').val(obj['data'][0]['user_name']);
              $$('#user_phone_edit_user').val(obj['data'][0]['user_phone']);
              $$('#email_edit_user').val(obj['data'][0]['email']);
              var store_id = obj['data'][0]['store_id'];
              if(store_id == "") {
                store_id = 0;
              }
              $$('#store_id_edit_user').val(store_id);
            } else {
              app.dialog.alert(obj['message'], 'Notifikasi', function(){
                page.router.navigate('/user/index');
              });
            }
          },
          error: function(data) {
            determinateLoading = false;
            app.dialog.close();
            var toastBottom = app.toast.create({
              text: error_connection,
              closeTimeout: 2000,
            });
            toastBottom.open();
            page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
          }
        });
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });
  
  $$('#btn_edit_user').on('click', function() {
    var username = $$('#username_edit_user').val();
    var user_name = $$('#user_name_edit_user').val();
    var user_phone = $$('#user_phone_edit_user').val();
    var email = $$('#email_edit_user').val();
    var password = $$('#password_edit_user').val();
    var store_id = $$('#store_id_edit_user').val();

    if(username == "") {
      app.dialog.alert('Username karyawan wajib diisi!');
    } else if(user_name == "") {
      app.dialog.alert('Nama karyawan wajib diisi!');
    } else if(user_phone == "") {
      app.dialog.alert('Telepon karyawan wajib diisi!');
    } else {
      app.request({
        method: "POST",
        url: urlxampp + "user/update.php", 
        data: {  
          user_id : user_id,
          username : username,
          user_name : user_name,
          user_phone : user_phone,
          email : email,
          password : password,
          store_id : store_id
        },
        success: function(data) {
          var obj = JSON.parse(data);
          determinateLoading = false;
          app.dialog.close();
          if(obj['status'] == true) {
            app.dialog.alert(obj['data'], 'Notifikasi', function(){
              page.router.navigate('/user/index');
            });
          } else {
            app.dialog.alert(obj['message']);
          }
        },
        error: function(data) {
          determinateLoading = false;
          app.dialog.close();
          var toastBottom = app.toast.create({
            text: error_connection,
            closeTimeout: 2000,
          });
          toastBottom.open();
          page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
        }
      });
    }
  });
}