function load_user_create(e, page) {
  // loadingData();

  app.request({
    method: "GET",
    url: urlxampp + "store/select.php", 
    data: {  
      
    },
    success: function(data) {
      var obj = JSON.parse(data);
      determinateLoading = false;
      app.dialog.close();
      if(obj['status'] == true) {
        var x = obj['data'];
        for(var i = 0; i < x.length; i++) {
          $$('#store_id_create_user').append(`
            <option value="` + x[i]['store_id'] + `">` + x[i]['store_name'] + `</option>
          `);
        }
      }
    },
    error: function(data) {
      determinateLoading = false;
      app.dialog.close();
      var toastBottom = app.toast.create({
        text: error_connection,
        closeTimeout: 2000,
      });
      toastBottom.open();
      page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
    }
  });
  
  $$('#btn_create_user').on('click', function() {
    var username = $$('#username_create_user').val();
    var user_name = $$('#user_name_create_user').val();
    var user_phone = $$('#user_phone_create_user').val();
    var email = $$('#email_create_user').val();
    var password = $$('#password_create_user').val();
    var store_id = $$('#store_id_create_user').val();

    if(username == "") {
      app.dialog.alert('Username karyawan wajib diisi!');
    } else if(user_name == "") {
      app.dialog.alert('Nama karyawan wajib diisi!');
    } else if(user_phone == "") {
      app.dialog.alert('Telepon karyawan wajib diisi!');
    } else if(password == "") {
      app.dialog.alert('Password karyawan wajib diisi!');
    } else {
      app.request({
        method: "POST",
        url: urlxampp + "user/insert.php", 
        data: {  
          username : username,
          user_name : user_name,
          user_phone : user_phone,
          email : email,
          password : password,
          user_role : 'Karyawan',
          store_id : store_id
        },
        success: function(data) {
          var obj = JSON.parse(data);
          determinateLoading = false;
          app.dialog.close();
          if(obj['status'] == true) {
            app.dialog.alert(obj['data'], 'Notifikasi', function(){
              page.router.navigate('/user/index');
            });
          } else {
            app.dialog.alert(obj['message']);
          }
        },
        error: function(data) {
          determinateLoading = false;
          app.dialog.close();
          var toastBottom = app.toast.create({
            text: error_connection,
            closeTimeout: 2000,
          });
          toastBottom.open();
          page.router.navigate('/user_home/',{ animate:false, reloadAll:true , force: true, ignoreCache: true});
        }
      });
    }
  });
}